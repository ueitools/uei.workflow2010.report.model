using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

namespace UEI.Workflow2010.Report.Model
{    
    public class Id
    {
        public Id(String _id)
        {
            _ID = _id;            
        }
        private String _ID = String.Empty;
        public String ID
        {
            get
            {
                return _ID;
            }
            set
            {
                _ID = value;
            }
        }
    }
    public class AliasId
    {
        public AliasId(String _id,String _aliasId)
        {
            _ID = _id;
            _AliasID = _aliasId;
        }
        private String _ID = String.Empty;
        public String ID
        {
            get{return _ID;}
            set{_ID = value;}
        }
        private String _AliasID = String.Empty;
        public String AliasID
        {
            get { return _AliasID; }
            set { _AliasID = value; }
        }
    }

    public class Country:IEquatable<Country>
    {
        private String _CountryID = String.Empty;
        public String CountryID
        {
            get { return _CountryID; }
            set { _CountryID = value; }
        }
        private String _Name = String.Empty;
        public String Name
        {
            get { return _Name; }
            set { _Name = value; }
        }
        public Country() { }
        public Country(String _countryID, String _country)
        {
            _CountryID = _countryID;
            _Name = _country;
        }
        public Country(String _countryID, String _country,String _subregionID, String _subregion)
        {
            _CountryID = _countryID;
            _Name = _country;
            _SubRegionID = _subregionID;
            _SubRegion = _subregion;
        }
        private String _SubRegionID = String.Empty;
        public String SubRegionID
        {
            get { return _SubRegionID; }
            set { _SubRegionID = value; }
        }
        private String _SubRegion = String.Empty;
        public String SubRegion
        {
            get { return _SubRegion; }
            set { _SubRegion = value; }
        }

        #region IEquatable<Country> Members

        public bool Equals(Country other)
        {
            if (String.Equals(this._CountryID, other._CountryID, StringComparison.OrdinalIgnoreCase) && String.Equals(this._Name, other._Name, StringComparison.OrdinalIgnoreCase))
                return true;
            else
                return false;
        }

        #endregion
    }
    public class CountryComparer : IComparer<Country>
    {
        #region IComparer<Country> Members
        public int Compare(Country x, Country y)
        {
            return x.Name.CompareTo(y.Name);
        }
        #endregion
    }
    public class Region:IEquatable<Region>
    {
        private String _RegionID = String.Empty;
        public String RegionID
        {
            get { return _RegionID; }
            set { _RegionID = value; }
        }        
        private String _Name = String.Empty;
        public String Name
        {
            get { return _Name; }
            set { _Name = value; }
        }
        public Region() { }
        public Region(String _regionID, String _region)
        {
            _RegionID = _regionID;
            _Name = _region;
        }
       	
        private List<Region> _SubRegions;
        public List<Region> SubRegions
        {
            get { return _SubRegions; }
            set { _SubRegions = value; }
        }
        private List<Country> _Countries;
        public List<Country> Countries
        {
            get { return _Countries; }
            set { _Countries = value; }
        }        
        #region IEquatable<Region> Members
        public bool Equals(Region other)
        {
            if (String.Equals(this._RegionID, other._RegionID, StringComparison.OrdinalIgnoreCase) && String.Equals(this._Name, other._Name, StringComparison.OrdinalIgnoreCase))
                return true;
            else
                return false;
        }

        #endregion
    }
    public class RegionComparer : IComparer<Region>
    {
        #region IComparer<Region> Members
        public int Compare(Region x, Region y)
        {
            return x.Name.CompareTo(y.Name);
        }
        #endregion
    }
    public class Location
    {
        private String m_Region = String.Empty;
        public String Region
        {
            get { return m_Region; }
            set { m_Region = value; }
        }
        private String m_SubRegion = String.Empty;
        public String SubRegion
        {
            get { return m_SubRegion; }
            set { m_SubRegion = value; }
        }
        private String m_AlternateRegion = String.Empty;
        public String AlternateRegion
        {
            get { return m_AlternateRegion; }
            set { m_AlternateRegion = value; }
        }
        private String m_Country = String.Empty;
        public String Country
        {
            get { return m_Country; }
            set { m_Country = value; }
        }
        public Location(String _region, String _subregion, String _alternateregion, String _country)
        {
            this.Region = _region;
            this.SubRegion = _subregion;
            this.AlternateRegion = _alternateregion;
            this.Country = _country;
        }
    }
    public class DeviceModel
    {
        private Dictionary<String, List<DeviceType>> items = new Dictionary<String, List<DeviceType>>();
        private DeviceType deviceToMatch = null;
        public bool IsDeviceAvailable
        {
            get {
                if (this.items.Count > 0)
                    return true;
                else
                    return false;
            }
        }
        public void AddDevice(DeviceType newDevice)
        {
            if (newDevice != null)
            {
                if (this.items.ContainsKey(newDevice.Mode))
                {
                    List<DeviceType> mainDevices = this.items[newDevice.Mode];
                    mainDevices.Add(newDevice);
                }
                else
                {
                    List<DeviceType> mainDevices = new List<DeviceType>();
                    mainDevices.Add(newDevice);
                    this.items.Add(newDevice.Mode, mainDevices);
                }
               
            }
        }
        public List<String> GetSubDevices(String m_Mode)
        {
            List<String> subdevices = new List<String>();
            Char[] checkValidModes = m_Mode.ToCharArray();            
            foreach (Char item in checkValidModes)
            {
                List<DeviceType> devices = GetDevices(item.ToString());
                foreach (DeviceType device in devices)
                {
                    foreach (DeviceType subdevice in device.SubDevices)
                    {
                        if (!subdevices.Contains(subdevice.Name))
                        {
                            subdevices.Add(subdevice.Name);
                        }
                    }
                }
            }
            if (subdevices.Count > 0)
                subdevices.Sort();
            return subdevices;
        }
        public DeviceType FindDevice(string mode,string deviceName)
        {
            //get the devicelist
            if (this.items.ContainsKey(mode))
            {
                List<DeviceType> deviceList = this.items[mode];
                if (deviceList != null)
                {
                    this.deviceToMatch = new DeviceType(mode, deviceName);
                    return deviceList.Find(MatchDevice);
                }
            }
            return null;
        }
        public List<DeviceType> GetDevices(string mode)
        {
            try
            {
                if (this.items[mode] != null)
                    return this.items[mode];
                else
                    return new List<DeviceType>();
            }
            catch
            {
                return new List<DeviceType>();
            }
        }
        private bool MatchDevice(DeviceType device)
        {
            if (this.deviceToMatch != null)
            {
                if (this.deviceToMatch.Name == device.Name && this.deviceToMatch.Mode == device.Mode)
                    return true;
                else
                    return false;
                
            }
            return false;
        }
    }    
    public class DeviceType:IEquatable<DeviceType>
    {
        private DeviceType deviceToMatch = null;
        private Component compToMatch = null;

        private String _DeviceTypeID = String.Empty;
        public String DeviceTypeID
        {
            get { return _DeviceTypeID; }
            set { _DeviceTypeID = value; }
        }
        private String _Name = String.Empty;
        public String Name
        {
            get { return _Name; }
            set { _Name = value; }
        }
        public DeviceType() { }
        public DeviceType(string mode, string name)
        {
            this._mode = mode;
            this._Name = name;
        }
        //public DeviceType(String deviceTypeID, String deviceType)
        //{
        //    _DeviceTypeID = deviceTypeID;
        //    _Name = deviceType;
        //}
        public DeviceType(String mode, String deviceTypeID, String deviceType)
        {
            _DeviceTypeID = deviceTypeID;
            _Name = deviceType;
            _mode = mode;
        }
        private String _mode = String.Empty;
        public String Mode
        {
            get { return _mode; }
            set { _mode = value; }
        }
        private List<DeviceType> _SubDevices = new List<DeviceType>();
        public List<DeviceType> SubDevices
        {
            get { return this._SubDevices; }
            set { this._SubDevices = value; }
        }

        private List<Component> _Components = new List<Component>();
        public List<Component> Components
        {
            get { return this._Components; }
            set { this._Components = value; }
        }

        private IList<AttributeGroup> _Attributes = new List<AttributeGroup>();
        public IList<AttributeGroup> Attributes
        {
            get { return this._Attributes; }
            set { this._Attributes = value; }
        }


        #region IEquatable<DeviceType> Members

        public bool Equals(DeviceType other)
        {
            if (String.Equals(this._Name, other._Name, StringComparison.OrdinalIgnoreCase))
                return true;
            else
                return false;
        }

        #endregion

        public DeviceType FindSubDevice(string subdeviceName)
        {
            if (this._SubDevices != null)
            {
                this.deviceToMatch = new DeviceType(this._mode, subdeviceName);
                return this._SubDevices.Find(MatchDevice);
            }
            return null;
        }
        public Component FindComponent(string componentName,string status)
        {
            if (this._Components != null)
            {
                this.compToMatch = new Component(componentName, status);
                return this._Components.Find(MatchComponent);
            }
            return null;
        }
        private bool MatchDevice(DeviceType device)
        {
            if (this.deviceToMatch != null)
            {
                if (this.deviceToMatch.Name == device.Name && this.deviceToMatch.Mode == device.Mode)
                    return true;
                else
                    return false;

            }
            return false;
        }
        private bool MatchComponent(Component component)
        {
            if (this.compToMatch != null)
            {
                if (this.compToMatch.Name == component.Name && this.compToMatch.Status == component.Status)
                    return true;
                else
                    return false;

            }
            return false;
        }
    }
    public class DeviceTypeComparer : IComparer<DeviceType>
    {
        #region IComparer<DeviceType> Members
        public int Compare(DeviceType x, DeviceType y)
        {
            return x.Name.CompareTo(y.Name);
        }
        #endregion
    }
    public class Component:IEquatable<Component>
    {
        private String _ComponentID = String.Empty;
        public String CompanentID
        {
            get { return _ComponentID; }
            set { _ComponentID = value; }
        }
        private String _Name = String.Empty;
        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }
        private String _SubDeviceTypeID = String.Empty;
        public String SubDeviceTypeID
        {
            get { return _SubDeviceTypeID; }
            set { _SubDeviceTypeID = value; }
        }
        private String _SubDeviceType = String.Empty;
        public String SubDeviceType
        {
            get { return _SubDeviceType; }
            set { _SubDeviceType = value; }
        }
        private string _Status;
        public string Status
        {
            get { return _Status; }
            set { _Status = value; }
        }	
        public Component() { }
        //public Component(String componentID, String component)
        //{
        //    CompanentID = componentID;
        //    Name = component;
        //}
        public Component(String component, String status)
        {
            this._Name = component;
            this._Status = status;
        }
        public Component(String componentID, String component, String subdevicetypeID, String subdevicetype)
        {
            CompanentID = componentID;
            Name = component;
            SubDeviceTypeID = subdevicetypeID;
            SubDeviceType = subdevicetype;
        }

        #region IEquatable<Component> Members

        public bool Equals(Component other)
        {
            if (String.Equals(this._ComponentID, other._ComponentID, StringComparison.OrdinalIgnoreCase) && String.Equals(this._Name, other._Name, StringComparison.OrdinalIgnoreCase))
                return true;
            else
                return false;
        }

        #endregion
    }
    public class ComponentComparer:IComparer<Component>
    {
        #region IComparer<Component> Members
        public int Compare(Component x, Component y)
        {
            return x.Name.CompareTo(y.Name);
        }
        #endregion
    }
    public class AttributeGroup
    {
        /// <summary>
        /// Group name
        /// </summary>
        public string Name
        {
            get
            {
                throw new System.NotImplementedException();
            }
            set
            {
            }
        }

        public string DeviceType
        {
            get
            {
                throw new System.NotImplementedException();
            }
            set
            {
            }
        }

        public List<Attribute> Attributes
        {
            get
            {
                throw new System.NotImplementedException();
            }
            set
            {
            }
        }
    }
    public class Attribute
    {
        public string Name
        {
            get
            {
                return null;   
            }
            set
            {
            }
        }

        public string GroupName
        {
            get
            {
                return null;
            }
            set
            {
            }
        }

        public List<string> Values
        {
            get
            {
                return null;  
            }
            set
            {
            }
        }
    }

    //public class SubDeviceType
    //{
    //}
    public class DataSources
    {
        private String _Name = String.Empty;
        public String Name
        {
            get { return _Name; }
            set { _Name = value; }
        }
        public DataSources() { }
        public DataSources(String _dataSourceLocation)
        {
            _Name = _dataSourceLocation;
        }
    }
    public class QAStatus
    {
        private Boolean m_FlagQ = true;
        public Boolean FlagQ
        {
            get { return m_FlagQ; }
            set { m_FlagQ = value; }
        }
        private Boolean m_FlagM = false;
        public Boolean FlagM
        {
            get { return m_FlagM; }
            set { m_FlagM = value; }
        }
        private Boolean m_FlagE = false;
        public Boolean FlagE
        {
            get { return m_FlagE; }
            set { m_FlagE = value; }
        }
        private Boolean m_FlagP = false;
        public Boolean FlagP
        {
            get { return m_FlagP; }
            set { m_FlagP = value; }
        }
        private Boolean m_TNStatus = false;
        public Boolean TNStatus
        {
            get { return m_TNStatus; }
            set { m_TNStatus = value; }
        }
        private Boolean m_Verbose = false;
        public Boolean Verbose
        {
            get { return m_Verbose; }
            set { m_Verbose = value; }
        }
    }
    public class SynthesizerTable
    {
        private String m_ID = String.Empty;
        public String ID
        {
            get { return m_ID; }
            set { m_ID = value; }
        }
        private String m_Label = String.Empty;
        public String Label
        {
            get { return m_Label; }
            set { m_Label = value; }
        }
        private String m_Syn = String.Empty;
        public String Syn
        {
            get { return m_Syn; }
            set { m_Syn = value; }
        }
        private String m_Intron = String.Empty;
        public String Intron
        {
            get { return m_Intron; }
            set { m_Intron = value; }
        }
        public SynthesizerTable(String id, String label, String syn, String intron)
        {
            this.ID = id;
            this.Label = label;
            this.Syn = syn;
            this.Intron = intron;
        }
    }
    public class BrandBasedSeach
    {
        private IList<String> m_Brand = new List<String>();
        public IList<String> Brand
        {
            get { return m_Brand; }
            set { m_Brand = value; }
        }
        //0: UEI2
        //1: UEI2 and POS
        //2: UEI2 and OSS
        private Int32 m_Avail = 0;
        public Int32 Avail
        {
            get { return m_Avail; }
            set { m_Avail = value; }
        }
        private Boolean m_Mode = false;
        public Boolean Mode
        {
            get { return m_Mode; }
            set { m_Mode = value; }
        }
        private Boolean m_Model = false;
        public Boolean Model
        {
            get { return m_Model; }
            set { m_Model = value; }
        }
        private Boolean m_DataSource = false;
        public Boolean DataSource
        {
            get { return m_DataSource; }
            set { m_DataSource = value; }
        }
        private String m_posStartDate = String.Empty;
        public String POSStartDate
        {
            get { return m_posStartDate; }
            set { m_posStartDate = value; }
        }
        private String m_POSEndDate = String.Empty;
        public String POSEndDate
        {
            get { return m_POSEndDate; }
            set { m_POSEndDate = value; }
        }
        private String m_OSSStartDate = String.Empty;
        public String OSSStartDate
        {
            get { return m_OSSStartDate; }
            set { m_OSSStartDate = value; }
        }
        private String m_OSSEndDate = String.Empty;
        public String OSSEndDate
        {
            get { return m_OSSEndDate; }
            set { m_OSSEndDate = value; }
        }
        private int _ModelType;
        public int ModelType
        {
            get { return _ModelType; }
            set { _ModelType = value; }
        }
        private Boolean _IncludeBlankModelNames = false;
        public Boolean IncludeBlankModelNames
        {
            get { return _IncludeBlankModelNames; }
            set { _IncludeBlankModelNames = value; }
        }
        private Boolean _InclideAliasBrand = false;
        public Boolean InclideAliasBrand
        {
            get { return _InclideAliasBrand; }
            set { _InclideAliasBrand = value; }    
        }
    }
    public class QuickSetModeSetup
    {        
        private String m_DeviceName = String.Empty;
        public String DeviceName
        {
            get { return m_DeviceName; }
            set { m_DeviceName = value; }
        }
        private String m_ModeList = String.Empty;
        public String ModeList
        {
            get { return m_ModeList; }
            set { m_ModeList = value; }
        }
        private List<String> m_SubDevices = new List<String>();
        public List<String> SubDevices
        {
            get { return m_SubDevices; }
            set { m_SubDevices = value; }
        }
        public QuickSetModeSetup(String deviceName, String modeList)
        {
            this.DeviceName = deviceName;
            this.ModeList = modeList;
        }
        public QuickSetModeSetup(String deviceName, String modeList,List<String> subdeviceList)
        {            
            this.DeviceName = deviceName;
            this.ModeList = modeList;
            this.SubDevices = subdeviceList;
        }        
    }
    public class QuickSetInfo
    {
        private Dictionary<Int32, QuickSetModeSetup> m_ModeSetup = new Dictionary<Int32, QuickSetModeSetup>();
        public Dictionary<Int32, QuickSetModeSetup> ModeSetup
        {
            get { return m_ModeSetup; }
            set { m_ModeSetup = value; }
        }
        private Dictionary<String,List<String>> m_PriorityIDList = new Dictionary<String,List<String>>();
        public Dictionary<String,List<String>> PriorityIDList
        {
            get { return m_PriorityIDList; }
            set { m_PriorityIDList = value; }
        }
        private Dictionary<String,String> m_AliasIDList = new Dictionary<String,String>();
        public Dictionary<String,String> AliasIDList
        {
            get { return m_AliasIDList; }
            set { m_AliasIDList = value; }
        }
        private ReportSortBy m_SortType = ReportSortBy.ModelCount;
        public ReportSortBy SortType
        {
            get { return m_SortType; }
            set { m_SortType = value; }
        }        
    }
    public class IdSetupCodeInfo
    {
        private Boolean m_IncludeModeChar = false;
        public Boolean IncludeModeChar
        {
            get { return m_IncludeModeChar; }
            set { m_IncludeModeChar = value; }
        }
        private Boolean m_IncludeUnsupportedPlatforms = true;
        public Boolean IncludeUnsupportedPlatforms
        {
            get { return m_IncludeUnsupportedPlatforms; }
            set { m_IncludeUnsupportedPlatforms = value; }
        }
        private IList<String> m_SupportedPlatforms = null;
        public IList<String> SupportedPlatforms
        {
            get { return m_SupportedPlatforms; }
            set { m_SupportedPlatforms = value; }
        }
        private Boolean m_IncludeNonComponentLinkedRecords = true;
        public Boolean IncludeNonComponentLinkedRecords
        {
            get { return m_IncludeNonComponentLinkedRecords; }
            set { m_IncludeNonComponentLinkedRecords = value; }
        }
        private Boolean m_IncludeEmptyId = false;
        public Boolean IncludeEmptyId
        {
            get { return m_IncludeEmptyId; }
            set { m_IncludeEmptyId = value; }
        }
        private IList<AliasId> _IdenticalIdList;
        public IList<AliasId> IdenticalIdList
        {
            get { return _IdenticalIdList; }
            set { _IdenticalIdList = value; }
        }
        private Boolean _IncludeIdenticalIdList = false;
        public Boolean IncludeIdenticalIdList
        {
            get { return _IncludeIdenticalIdList; }
            set { _IncludeIdenticalIdList = value; }
        }
        private String m_startDate = String.Empty;
        public String StartDate
        {
            get { return m_startDate; }
            set { m_startDate = value; }
        }
        private String m_endDate = String.Empty;
        public String EndDate
        {
            get { return m_endDate; }
            set { m_endDate = value; }
        }
        private Boolean _IncludeEmptyDeviceType = true;
        public Boolean IncludeEmptyDeviceType
        {
            get { return _IncludeEmptyDeviceType; }
            set { _IncludeEmptyDeviceType = value; }
        }
        private String _componentStatus = String.Empty;
        public String ComponentStatus
        {
            get { return _componentStatus; }
            set { _componentStatus = value; }
        }
        private Boolean _IncludeNonCountryLinkedRecords = true;
        public Boolean IncludeNonCountryLinkedRecords
        {
            get { return _IncludeNonCountryLinkedRecords; }
            set { _IncludeNonCountryLinkedRecords = value; }
        }
        private Boolean _IncludeUnknownLocation = true;
        public Boolean IncludeUnknownLocation
        {
            get { return _IncludeUnknownLocation; }
            set { _IncludeUnknownLocation = value; }
        }
        private String _IncludeRestrictedIds = "1";
        public String IncludeRestrictedIds
        {
            get { return _IncludeRestrictedIds.ToUpper(); }
            set { _IncludeRestrictedIds = value.ToUpper(); }
        }
        private Boolean _DisplayMajorBrandsOnly = false;
        public Boolean DisplayMajorBrandsOnly
        {
            get { return _DisplayMajorBrandsOnly; }
            set { _DisplayMajorBrandsOnly = value; }
        }
        private Boolean _DisplayBrandNumber = false;
        public Boolean DisplayBrandNumber
        {
            get { return _DisplayBrandNumber; }
            set { _DisplayBrandNumber = value; }
        }
        private Boolean _IncludeBlankModelNames = true;
        public Boolean IncludeBlankModelNames
        {
            get { return _IncludeBlankModelNames; }
            set { _IncludeBlankModelNames = value; }
        }
        private Boolean _IncludeAliasBrand = true;
        public Boolean InclideAliasBrand
        {
            get { return _IncludeAliasBrand; }
            set { _IncludeAliasBrand = value; }
        }
        private Int32 _IdOffset = 0;
        public Int32 IdOffset
        {
            get { return _IdOffset; }
            set { _IdOffset = value; }
        }
        private String _SetupCodeFormat = "Base 10";
        public String SetupCodeFormat
        {
            get { return _SetupCodeFormat; }
            set { _SetupCodeFormat = value; }
        }
        private int _ModelType;
        public int ModelType
        {
            get { return _ModelType; }
            set { _ModelType = value; }
        }	
        private ReportSortBy _SortType = ReportSortBy.ModelCount;
        public ReportSortBy SortType
        {
            get { return _SortType; }
            set { _SortType = value; }
        }
        private IList<Id> _PriorityIDList;
        public IList<Id> PriorityIDList
        {
            get { return _PriorityIDList; }
            set { _PriorityIDList = value; }
        }
        private IList<AliasId> _AliasIDList;
        public IList<AliasId> AliasIDList
        {
            get { return _AliasIDList; }
            set { _AliasIDList = value; }
        }        
        private Boolean _SetupCodeFormatErrorFlag = false;
        public Boolean SetupCodeFormatErrorFlag
        {
            get { return _SetupCodeFormatErrorFlag; }
            set { _SetupCodeFormatErrorFlag = value; }
        }

        private int _StartingDigitofSetupCode = 0;

        public int StartingDigitofSetupCode
        {
            get { return _StartingDigitofSetupCode; }
            set { _StartingDigitofSetupCode = value; }
        }

        private DataSet _SetupCodesWithFormatError = new System.Data.DataSet();
        public void SetSetupCodesWithFormatError(string brand, string setupCodes)
        {
            DataTable dt = null;
            if (this._SetupCodesWithFormatError.Tables.Count == 0)
            {
                dt = this._SetupCodesWithFormatError.Tables.Add("FormatErrorCodes");
                dt.Columns.Add("Brand");
                dt.Columns.Add("SetupCodes");
            }
            else
                dt = this._SetupCodesWithFormatError.Tables[0];

            DataRow newRow = dt.NewRow();
            newRow["Brand"] = brand;
            newRow["SetupCodes"] = setupCodes;

            dt.Rows.Add(newRow);

            this._SetupCodesWithFormatError.AcceptChanges();


        }
        public DataSet GetSetupCodesWithFormatError()
        {
            return this._SetupCodesWithFormatError;
        }             
    }    
    public class BrandFilter
    {
        private bool _RecordWithoutBrandNumber = false;        
        public bool RecordWithoutBrandNumber
        {
            get { return _RecordWithoutBrandNumber; }
            set { _RecordWithoutBrandNumber = value; }
        }        
        private bool _MatchSelectedBrands = false;        
        public bool MatchSelectedBrands
        {
            get { return _MatchSelectedBrands; }
            set { this._MatchSelectedBrands = value; }
        }
        private List<String> _SelectedBrands = new List<string>();
        public List<String> SelectedBrands
        {
            get { return _SelectedBrands; }
            set { _SelectedBrands = value; }
        }
    }    
    public class ModelFilter
    {
        private bool _RecordWithoutModelName;
        public bool RecordWithoutModelName
        {
            get { return _RecordWithoutModelName; }
            set { _RecordWithoutModelName = value; }
        }
        private bool _RemoveNonCodeBookModels;
        public bool RemoveNonCodeBookModels
        {
            get { return _RemoveNonCodeBookModels; }
            set { _RemoveNonCodeBookModels = value; }
        }
        private bool _MatchSelectedModels;
        public bool MatchSelectedModels
        {
            get { return _MatchSelectedModels; }
            set { _MatchSelectedModels = value; }
        }
        private List<string> _SelectedModels = new List<string>();
        public List<string> SelectedModels
        {
            get { return _SelectedModels; }
            set { _SelectedModels = value; }
        }
    }    
    public class ModelInfoSpecific
    {
        private Boolean m_QuickSetFormat = false;
        public Boolean QuickSetFormat
        {
            get { return m_QuickSetFormat; }
            set { m_QuickSetFormat = value; }
        }
        private String m_RowSelectorName = String.Empty;
        public String RowSelectorName 
        {
            get { return m_RowSelectorName; }
            set { m_RowSelectorName = value; }
        }
        private Dictionary<String, Boolean> _RowSelector;
        public Dictionary<String, Boolean> RowSelector
        {
            get { return _RowSelector; }
            set { _RowSelector = value; }
        }
        private Dictionary<String,Boolean> _ColumnSelector;
        public Dictionary<String,Boolean> ColumnSelector
        {
            get { return _ColumnSelector; }
            set { _ColumnSelector = value; }
        }
        public enum IsTargetFilter
        {
            ShowTargetModelOnly,
            ShowRemoteModelOnly,
            ShowTargetAndRemote
        }
        //public enum DisplayFilter
        //{
        //    ShowAll=0,
        //    Distinct=1
        //}
        public enum ModelReportOptions
        {
            ModelBased =0,
            TNBased = 1,
            BrandBased = 2,
            IDBased=3
        }
        private bool _KeepDuplicatedOutputs = false;
        public bool KeepDuplicatedOutputs
        {
            get { return this._KeepDuplicatedOutputs; }
            set { this._KeepDuplicatedOutputs = value;}
        }
        private bool _SelectTNLink;
        public bool SelectTNLink
        {
            get { return _SelectTNLink; }
            set { _SelectTNLink = value; }
        }
        private IsTargetFilter _IsTargetFilter = IsTargetFilter.ShowTargetAndRemote;
        public IsTargetFilter TargetModelFilter
        {
            get { return _IsTargetFilter; }
            set { _IsTargetFilter = value; }
        }
        private DisplayFilter _displayMode = DisplayFilter.ShowAll;
        public DisplayFilter DisplayMode
        {
            get { return _displayMode; }
            set { _displayMode = value; }
        }		
        private bool _EnableDateBasedSearch;
        public bool DateBasedSearchEnabled
        {
            get { return _EnableDateBasedSearch; }
            set { _EnableDateBasedSearch = value; }
        }
        private string _FromDate;
        public string FromDate
        {
            get { return _FromDate; }
            set { _FromDate = value; }
        }
        private string _ToDate;
        public string ToDate
        {
            get { return _ToDate; }
            set { _ToDate = value; }
        }
        private ModelReportOptions _ModelInfoOption;
        public ModelReportOptions ModelInfoOptions
        {
            get { return _ModelInfoOption; }
            set { _ModelInfoOption = value; }
        }
        private String _IncludeRestrictedIds = "1";
        public String IncludeRestrictedIds
        {
            get { return _IncludeRestrictedIds.ToUpper(); }
            set { _IncludeRestrictedIds = value.ToUpper(); }
        }
        private IList<AliasId> _IdenticalIdList;
        public IList<AliasId> IdenticalIdList
        {
            get { return _IdenticalIdList; }
            set { _IdenticalIdList = value; }
        }
        private Boolean _IncludeIdenticalIdList = false;
        public Boolean IncludeIdenticalIdList
        {
            get { return _IncludeIdenticalIdList; }
            set { _IncludeIdenticalIdList = value; }
        }
    }
    public class LabelIntronFilters
    {
       
        private bool isIntronSearch = false;

        public bool IsIntronSearch
        {
            get { return isIntronSearch; }
            set { isIntronSearch = value; }
        }

        private string searchPatterns = String.Empty;

        public string SearchPattern
        {
            get { return searchPatterns; }
            set { searchPatterns = value; }
        }

        private bool isCompleteMatch = false;

        public bool IsCompleteMatch
        {
            get { return isCompleteMatch; }
            set { isCompleteMatch = value; }
        }

        private DisplayFilter _displayMode = DisplayFilter.ShowAll;

        public DisplayFilter DisplayMode
        {
            get { return _displayMode; }
            set { _displayMode = value; }
        }

    }
    public enum ReportSortBy
    {
        ModelCount,
        AlphabeticalBrandNames,
        IDNumber,
        PriorityIDListFile,
        POS,
        SearchStatistics
    }
    public enum DisplayFilter
    {
        ShowAll = 0,
        Distinct = 1
    }
}
