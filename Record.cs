using System;
using System.Collections.Generic;
using System.Text;
using System.Collections.ObjectModel;
using System.Data;
using System.Collections;

namespace UEI.Workflow2010.Report.Model
{
    public class KeyFunctionModel
    {
        private Dictionary<String, List<KeyFunctionRecord>> m_KeyFunctions = new Dictionary<String, List<KeyFunctionRecord>>();
        public Dictionary<String, List<KeyFunctionRecord>> KeyFunctions
        {
            get { return m_KeyFunctions; }
            set { m_KeyFunctions = value; }
        }
        public DataSet GetKeyFunctionReportDataSetForExcel(Int32 keyFunctionReportType, IDModeRecordModel idModeRecordModel)
        {
            DataSet ds = null;
            if (this.KeyFunctions != null)
            {
                ds = new DataSet("KeyFunction");                
                foreach (String mode in this.KeyFunctions.Keys)
                {
                    ds.Tables.Add(mode);
                    switch (keyFunctionReportType)
                    {
                        case 1:
                            ds.Tables[mode].Columns.Add("Mode");
                            ds.Tables[mode].Columns.Add("Function");
                            ds.Tables[mode].Columns.Add("Count", Type.GetType("System.Int32"));
                            break;
                        case 2:
                            ds.Tables[mode].Columns.Add("Mode");
                            ds.Tables[mode].Columns.Add("ID");
                            ds.Tables[mode].Columns.Add("Function");
                            ds.Tables[mode].Columns.Add("Count", Type.GetType("System.Int32"));
                            break;
                        case 3:
                            ds.Tables[mode].Columns.Add("Mode");
                            ds.Tables[mode].Columns.Add("ID");
                            ds.Tables[mode].Columns.Add("Function");
                            ds.Tables[mode].Columns.Add("MainDevice");
                            ds.Tables[mode].Columns.Add("SubDevice");
                            ds.Tables[mode].Columns.Add("Count", Type.GetType("System.Int32"));
                            break;
                    }
                    ds.Tables[mode].AcceptChanges();

                    String modeName = idModeRecordModel.getModeName(mode);
                    DataRow dr = null;
                    dr = ds.Tables[mode].NewRow();
                    dr[0] = modeName;
                    ds.Tables[mode].Rows.Add(dr);
                    dr = null;
                    KeyFunctionComparer m_Comparer = new KeyFunctionComparer();
                    this.KeyFunctions[mode].Sort(m_Comparer);
                    foreach (KeyFunctionRecord record in this.KeyFunctions[mode])
                    {
                        dr = ds.Tables[mode].NewRow();
                        dr["Mode"] = mode;
                        dr["Function"] = record.Function;
                        dr["Count"] = record.FunctionCount;
                        if (ds.Tables[mode].Columns.Contains("ID"))
                            dr["ID"] = record.ID;
                        if (ds.Tables[mode].Columns.Contains("MainDevice"))
                            dr["MainDevice"] = record.MainDevice;
                        if (ds.Tables[mode].Columns.Contains("SubDevice"))
                            dr["SubDevice"] = record.SubDevice;
                        ds.Tables[mode].Rows.Add(dr);
                        dr = null;
                    }
                }
                ds.AcceptChanges();
            }
            return ds;
        }
        public DataSet GetKeyFunctionReportDataSet(Int32 keyFunctionReportType, IDModeRecordModel idModeRecordModel)
        {
            DataSet ds = null;
            if (this.KeyFunctions != null)
            {
                ds = new DataSet("KeyFunctionReport");
                ds.Tables.Add("KeyFunctionCount");
                switch (keyFunctionReportType)
                {
                    case 1:
                        ds.Tables["KeyFunctionCount"].Columns.Add("Mode");                        
                        ds.Tables["KeyFunctionCount"].Columns.Add("Function");
                        ds.Tables["KeyFunctionCount"].Columns.Add("Count",Type.GetType("System.Int32"));
                        break;
                    case 2:
                        ds.Tables["KeyFunctionCount"].Columns.Add("Mode");
                        ds.Tables["KeyFunctionCount"].Columns.Add("ID");
                        ds.Tables["KeyFunctionCount"].Columns.Add("Function");
                        ds.Tables["KeyFunctionCount"].Columns.Add("Count",Type.GetType("System.Int32"));
                        break;
                    case 3:
                        ds.Tables["KeyFunctionCount"].Columns.Add("Mode");
                        ds.Tables["KeyFunctionCount"].Columns.Add("ID");
                        ds.Tables["KeyFunctionCount"].Columns.Add("Function");
                        ds.Tables["KeyFunctionCount"].Columns.Add("MainDevice");
                        ds.Tables["KeyFunctionCount"].Columns.Add("SubDevice");
                        ds.Tables["KeyFunctionCount"].Columns.Add("Count",Type.GetType("System.Int32"));
                        break;
                }
                ds.Tables["KeyFunctionCount"].AcceptChanges();
                foreach (String mode in this.KeyFunctions.Keys)
                {
                    String modeName = idModeRecordModel.getModeName(mode);
                    DataRow dr = null;
                    dr = ds.Tables["KeyFunctionCount"].NewRow();
                    dr[0] = modeName;
                    ds.Tables["KeyFunctionCount"].Rows.Add(dr);
                    dr = null;
                    KeyFunctionComparer m_Comparer = new KeyFunctionComparer();
                    this.KeyFunctions[mode].Sort(m_Comparer);
                    foreach (KeyFunctionRecord record in this.KeyFunctions[mode])
                    {
                        dr = ds.Tables["KeyFunctionCount"].NewRow();
                        dr["Mode"] = mode;
                        dr["Function"] = record.Function;
                        dr["Count"] = record.FunctionCount;
                        if (ds.Tables["KeyFunctionCount"].Columns.Contains("ID"))
                            dr["ID"] = record.ID;
                        if (ds.Tables["KeyFunctionCount"].Columns.Contains("MainDevice"))
                            dr["MainDevice"] = record.MainDevice;
                        if (ds.Tables["KeyFunctionCount"].Columns.Contains("SubDevice"))
                            dr["SubDevice"] = record.SubDevice;
                        ds.Tables["KeyFunctionCount"].Rows.Add(dr);
                        dr = null;
                    }
                }
                ds.AcceptChanges();
            }
            return ds;
        }
    }
    public class KeyFunctionRecord
    {
        public String ID { get; set; }
        public String Function { get; set; }
        public String MainDevice { get; set; }
        public String SubDevice { get; set; }
        public Int32 FunctionCount { get; set; }
    }
    public class KeyFunctionComparer : IComparer<KeyFunctionRecord>
    {
        #region IComparer<KeyFunctionRecord> Members
        public int Compare(KeyFunctionRecord x, KeyFunctionRecord y)
        {
            return y.FunctionCount.CompareTo(x.FunctionCount);
        }
        #endregion
    }
    public class ExIdMap
    {
        private SortedDictionary<String, List<String>> m_Ids = new SortedDictionary<String, List<String>>();
        public SortedDictionary<String, List<String>> Ids
        {
            get { return m_Ids; }
            set { m_Ids = value; }
        }
        public void Add(String id,String brand)
        {
            if (!this.Ids.ContainsKey(id))
                this.Ids.Add(id, new List<String>() { brand });
            else
                if (!this.Ids[id].Contains(brand))
                    this.Ids[id].Add(brand);
        }
    }
    public class BrandItem
    {
        private String m_Brand = String.Empty;
        public String Brand
        {
            get { return m_Brand; }
            set { m_Brand = value; }
        }
        private Int32 m_BrandNum = 0;
        public Int32 BrandNum
        {
            get { return m_BrandNum; }
            set { m_BrandNum = value; }
        }
        private IList<String> m_IDPriority = new List<String>();
        public IList<String> IDPriority
        {
            get { return m_IDPriority; }
            set { m_IDPriority = value; }
        }
        private IList<String> m_IDList = new List<String>();
        public IList<String> IDList
        {
            get { return m_IDList; }
            set { m_IDList = value; }
        }
    }
    public class ModeBrandItem
    {
        private String m_Mode = String.Empty;
        public String Mode
        {
            get { return m_Mode; }
            set { m_Mode = value; }
        }
        private IList<BrandItem> m_BrandInfo = new List<BrandItem>();
        public IList<BrandItem> BrandInfo
        {
            get { return m_BrandInfo; }
            set { m_BrandInfo = value; }
        }
    }

    public class BrandSearchRecord
    {
        private String m_Brand = String.Empty;
        public String Brand
        {
            get { return m_Brand; }
            set { m_Brand = value; }
        }
        private Int32 m_BrandType = 0;
        public Int32 BrandType
        {
            get { return m_BrandType; }
            set { m_BrandType = value; }
        }
        private String m_Mode = String.Empty;
        public String Mode
        {
            get { return m_Mode; }
            set { m_Mode = value; }
        }
        private String m_DataSource = String.Empty;
        public String DataSource
        {
            get { return m_DataSource; }
            set { m_DataSource = value; }
        }
        private String m_Region = String.Empty;
        public String Region
        {
            get { return m_Region; }
            set { m_Region = value; }
        }
        private String m_Country = String.Empty;
        public String Country
        {
            get { return m_Country; }
            set { m_Country = value; }
        }
    }
    public class LocationModel : Collection<Location>
    {
        public List<Region> GetRegions()
        {
            List<Region> regionList = new List<Region>();
            if (this.Items != null)
            {
                foreach (Location item in this.Items)
                {
                    if (item.Region.Trim() != String.Empty)
                    {
                        Region newRegion = new Region(item.Region, item.Region);
                        if (regionList == null)
                        {
                            regionList.Add(newRegion);
                        }
                        else if (!regionList.Contains(newRegion))
                        {
                            regionList.Add(newRegion);
                        }
                    }
                }
                RegionComparer comparer = new RegionComparer();
                regionList.Sort(comparer);
            }
            return regionList;
        }
        public List<Region> GetSubRegions(Region region)
        {
            List<Region> subregionList = new List<Region>();
            if (this.Items != null)
            {
                foreach (Location item in this.Items)
                {
                    if (item.Region == region.Name && item.SubRegion.Trim() != String.Empty)
                    {
                        Region newsubRegion = new Region(item.SubRegion, item.SubRegion);
                        if (subregionList == null)
                        {
                            subregionList.Add(newsubRegion);
                        }
                        else if (!subregionList.Contains(newsubRegion))
                        {
                            subregionList.Add(newsubRegion);
                        }
                    }                    
                }
                RegionComparer comparer = new RegionComparer();
                subregionList.Sort(comparer);
            }
            return subregionList;
        }

        public List<Country> GetCountries(Region region, Region subreg)
        {
            List<Country> countryList = new List<Country>();
            if (this.Items != null)
            {
                if (subreg == null)
                {
                    foreach (Location item in this.Items)
                    {
                        if (item.Region == region.Name && item.SubRegion == String.Empty && item.Country.Trim() != String.Empty)
                        {                            
                            Country newCountry = new Country(item.Country, item.Country);
                            if (countryList == null)
                            {
                                countryList.Add(newCountry);
                            }
                            else if (!countryList.Contains(newCountry))
                            {
                                countryList.Add(newCountry);
                            }
                        }
                    }
                }
                else
                {
                    foreach (Location item in this.Items)
                    {
                        if (item.Region == region.Name && item.SubRegion == subreg.Name && item.Country.Trim() != String.Empty)
                        {
                            Country newCountry = new Country(item.Country, item.Country);
                            if (countryList == null)
                            {                             
                                countryList.Add(newCountry);
                            }
                            else if (!countryList.Contains(newCountry))
                            {
                                countryList.Add(newCountry);
                            }
                        }
                    }
                }
                CountryComparer comparer = new CountryComparer();
                countryList.Sort(comparer);
            }
            return countryList;
        }
        public List<Country> GetCountriesWithoutSubDevices(Region region)
        {
            List<Country> countryList = new List<Country>();
            if (this.Items != null)
            {
                foreach (Location item in this.Items)
                {
                    if (item.Region == region.Name && item.SubRegion == String.Empty && item.Country.Trim() != String.Empty)
                    {
                        Country newCountry = new Country(item.Country, item.Country);
                        if (countryList == null)
                        {
                            countryList.Add(newCountry);
                        }
                        else if (!countryList.Contains(newCountry))
                        {
                            countryList.Add(newCountry);
                        }
                    }
                }
                CountryComparer comparer = new CountryComparer();
                countryList.Sort(comparer);
            }
            return countryList;
        }
    }
    public class BrandSearchModel : Collection<BrandSearchRecord>
    {
        public DataSet GetBrandSearchResult()
        {
            DataSet reportSet = null;
            if (this.Items != null)
            {
                reportSet = new DataSet("BrandSearch");
                DataTable dtReport = reportSet.Tables.Add("Brands");
                dtReport.Columns.Add("Brand");
                dtReport.Columns.Add("Standard Brand");
                dtReport.Columns.Add("Alias Brand");
                dtReport.Columns.Add("Variation Brand");
                dtReport.Columns.Add("New Brand");
                dtReport.Columns.Add("Mode");
                dtReport.Columns.Add("DataSource");
                dtReport.Columns.Add("Region");
                dtReport.Columns.Add("Country");
                dtReport.AcceptChanges();
                DataRow dr = null;
                foreach (BrandSearchRecord item in this.Items)
                {
                    dr = dtReport.NewRow();
                    dr[0] = item.Brand;
                    if (item.BrandType == 0)
                    {
                        dr[1] = "Y";
                    }
                    else if (item.BrandType == 1)
                    {
                        dr[2] = "Y";
                    }
                    else if (item.BrandType == 2)
                    {
                        dr[3] = "Y";
                    }
                    else if (item.BrandType == 3)
                    {
                        dr[4] = "Y";
                    }
                    dr[5] = item.Mode;
                    dr[6] = item.DataSource;
                    dr[7] = item.Region;
                    dr[8] = item.Country;
                    dtReport.Rows.Add(dr);
                    dr = null;
                }
                dtReport.AcceptChanges();
                //reportSet.Tables.Add(dtReport);
                reportSet.AcceptChanges();
            }
            return reportSet;
        }
    }
    public class BrandBasedSearchRecord
    {
        private String m_Brand = String.Empty;
        public String Brand
        {
            get { return m_Brand; }
            set { m_Brand = value; }
        }
        private String m_Mode = String.Empty;
        public String Mode
        {
            get { return m_Mode; }
            set { m_Mode = value; }
        }
        private List<DeviceInfo> m_DeviceType = new List<DeviceInfo>();
        public List<DeviceInfo> DeviceTypes
        {
            get { return m_DeviceType; }
            set { m_DeviceType = value; }
        }        
    }
    public class BrandBasedSearchModel : Collection<BrandBasedSearchRecord>
    {
        public BrandBasedSearchRecord GetBrandSearch(String m_Brand)
        {
            foreach (BrandBasedSearchRecord item in this.Items)
            {
                if (item.Brand == m_Brand)
                    return item;
            }
            return null;
        }

        public BrandBasedSearchRecord GetBrandSearch(String brand, String mode)
        {
            foreach (BrandBasedSearchRecord item in this.Items)
            {
                if (item.Brand.ToUpper() == brand.ToUpper() && item.Mode.ToUpper() == mode.ToUpper())
                    return item;
            }
            return null;
        }

        public DeviceInfo GetBrandSearch(List<DeviceInfo> deviceList, String deviceType)
        {
            foreach (DeviceInfo item in deviceList)
            {
                if (item.Name.ToUpper() == deviceType.ToUpper())
                    return item;
            }
            return null;
        }
        public IList<String> GetDistinctModes()
        {
            IList<String> distinctModes = new List<String>();
            foreach (BrandBasedSearchRecord item in this.Items)
            {
                if (!distinctModes.Contains(item.Mode))
                {
                    distinctModes.Add(item.Mode);
                }
            }
            return distinctModes;
        }
        public IList<String> GetDistinctBrands()
        {
            IList<String> distinctBrands = new List<String>();
            foreach (BrandBasedSearchRecord item in this.Items)
            {
                if (!distinctBrands.Contains(item.Brand))
                {
                    distinctBrands.Add(item.Brand);
                }
            }
            return distinctBrands;
        }
        public IList<string> GetDistinctBrands(String m_Modes)
        {
            IList<String> distinctBrands = new List<String>();
            foreach (BrandBasedSearchRecord item in this.Items)
            {
                if (!distinctBrands.Contains(item.Brand) && item.Mode == m_Modes)
                {
                    distinctBrands.Add(item.Brand);
                }
            }
            return distinctBrands;
        }
        public String GetIdList(String m_Brand)
        {
            List<IDInfo> idList = new List<IDInfo>();
            foreach (BrandBasedSearchRecord item in this.Items)
            {
                if (item.Brand == m_Brand)
                {
                    foreach (DeviceInfo deviceInfo in item.DeviceTypes)
                    {
                        foreach (IDInfo idInfo in deviceInfo.IdList)
                        {
                            if(!IsIdExists(idList,idInfo))
                                idList.Add(idInfo);
                        }
                    }
                }
            }
            if (idList.Count > 0)
            {
                Model.IDComparer comparer = new IDComparer(ReportSortBy.ModelCount);
                idList.Sort(comparer);
            }
            StringBuilder ids = new StringBuilder();
            //Setup Code format is set to "Base 10"
            foreach (IDInfo id in idList)
            {
                ids.Append(String.Format("{0:0000}", id.ID));
                ids.Append(", ");
            }
            if (ids.Length > 0)
                ids.Remove(ids.Length - 2, 2);
            return ids.ToString();
        }
        private Boolean IsIdExists(List<IDInfo> idList, IDInfo idInfo)
        {
            foreach (IDInfo ids in idList)
            {
                if (ids.ID == idInfo.ID)
                    return true;
            }
            return false;
        }
        public String GetIdList(String m_Brand, String m_DataSource)
        {
            List<IDInfo> idList = new List<IDInfo>();
            foreach (BrandBasedSearchRecord item in this.Items)
            {
                if (item.Brand == m_Brand)
                {
                    foreach (DeviceInfo deviceInfo in item.DeviceTypes)
                    {
                        foreach (IDInfo idInfo in deviceInfo.IdList)
                        {
                            foreach (ModelInfo modelInfo in idInfo.ModelList)
                            {
                                foreach (DataSourceInfo datasourceInfo in modelInfo.DataSources)
                                {
                                    if ((datasourceInfo.Name == m_DataSource) && (!IsIdExists(idList,idInfo)))
                                        idList.Add(idInfo);
                                }
                            }                            
                        }
                    }
                }
            }
            if (idList.Count > 0)
            {
                Model.IDComparer comparer = new IDComparer(ReportSortBy.ModelCount);
                idList.Sort(comparer);
            }
            StringBuilder ids = new StringBuilder();
            //Setup Code format is set to "Base 10"
            foreach (IDInfo id in idList)
            {
                ids.Append(String.Format("{0:0000}", id.ID));
                ids.Append(", ");
            }
            if (ids.Length > 0)
                ids.Remove(ids.Length - 2, 2);
            return ids.ToString();
        }
        private Boolean IsModelExist(List<ModelInfo> modelList, ModelInfo modelInfo)
        {
            foreach (ModelInfo model in modelList)
            {
                if (model.Name == modelInfo.Name)
                    return true;
            }
            return false;
        }
        public String GetModelList(String m_Brand)
        {
            List<ModelInfo> modelList = new List<ModelInfo>();
            foreach (BrandBasedSearchRecord item in this.Items)
            {
                if (item.Brand == m_Brand)
                {
                    foreach (DeviceInfo deviceInfo in item.DeviceTypes)
                    {
                        foreach (IDInfo idInfo in deviceInfo.IdList)
                        {
                            foreach (ModelInfo modelInfo in idInfo.ModelList)
                            {
                                if (modelInfo != null && !IsModelExist(modelList,modelInfo))
                                    modelList.Add(modelInfo);
                            }                            
                        }
                    }
                }
            }
            if (modelList.Count > 0)
            {
                Model.ModelComparer comparer = new ModelComparer(ReportSortBy.ModelCount);
                modelList.Sort(comparer);
            }
            StringBuilder models = new StringBuilder();
            foreach (ModelInfo model in modelList)
            {
                if (model.Name.Trim() != String.Empty)
                {
                    models.Append(model.Name);
                    models.Append(", ");
                }
            }
            if (models.Length > 0)
                models.Remove(models.Length - 2, 2);
            return models.ToString();
        }
        public String GetModelList(String m_Brand, String m_DataSource)
        {
            List<ModelInfo> modelList = new List<ModelInfo>();
            foreach (BrandBasedSearchRecord item in this.Items)
            {
                if (item.Brand == m_Brand)
                {
                    foreach (DeviceInfo deviceInfo in item.DeviceTypes)
                    {
                        foreach (IDInfo idInfo in deviceInfo.IdList)
                        {
                            foreach (ModelInfo modelInfo in idInfo.ModelList)
                            {
                                if (modelInfo != null)
                                {
                                    foreach (DataSourceInfo dataSource in modelInfo.DataSources)
                                    {
                                        if ((dataSource.Name == m_DataSource)&&(!IsModelExist(modelList,modelInfo)))
                                            modelList.Add(modelInfo);
                                    }                                    
                                }
                            }
                        }
                    }
                }
            }
            Model.ModelComparer comparer = new ModelComparer(ReportSortBy.ModelCount);
            modelList.Sort(comparer);
            StringBuilder models = new StringBuilder();
            foreach (ModelInfo model in modelList)
            {
                if (model.Name.Trim() != String.Empty)
                {
                    models.Append(model.Name);
                    models.Append(", ");
                }
            }
            if (models.Length > 0)
                models.Remove(models.Length - 2, 2);
            return models.ToString();
        }
        public IList<String> GetDistinctDataSources(String m_Brand)
        {
            IList<String> distinctDataSources = new List<String>();
            foreach (BrandBasedSearchRecord item in this.Items)
            {
                if(item.Brand == m_Brand)
                    foreach (DeviceInfo deviceInfo in item.DeviceTypes)
                    {
                        foreach (IDInfo idInfo in deviceInfo.IdList)
                        {
                            foreach (ModelInfo modelInfo in idInfo.ModelList)
                            {
                                foreach (DataSourceInfo datascourceInfo in modelInfo.DataSources)
                                {
                                    if (!distinctDataSources.Contains(datascourceInfo.Name))
                                        distinctDataSources.Add(datascourceInfo.Name);
                                }
                            }
                        }
                    }                
            }
            return distinctDataSources;
        }
    }
    public class IDModeRecord : IComparable<IDModeRecord>
    {
        private String m_Mode = String.Empty;
        public String Mode
        {
            get { return m_Mode; }
            set { m_Mode = value; }
        }
        private String m_Name = String.Empty;
        public String Name
        {
            get { return m_Name; }
            set { m_Name = value; }
        }
        public IDModeRecord() { }
        public IDModeRecord(String mode, String name) {
            this.m_Mode = mode;
            this.m_Name = name;
        }
        public int CompareTo(IDModeRecord other)
        {
            return this.Mode.CompareTo(other.Mode);
        }
    }
    public class UniqueFuncIRCountModel : Collection<UniqueFuncIRCountRecord>
    {
    }
    public class UniqueFuncIRCountRecord
    {
        private String m_Mode = String.Empty;
        public String Mode
        {
            get { return m_Mode; }
            set { m_Mode = value; }
        }
        private Int32 m_TotalFuncttions = 0;
        public Int32 TotalFunctions
        {
            get { return m_TotalFuncttions; }
            set { m_TotalFuncttions = value; }
        }
        private Int32 m_TotalIRCodes = 0;
        public Int32 TotalIRCodes
        {
            get { return m_TotalIRCodes; }
            set { m_TotalIRCodes = value; }
        }
    }   
    public class IDModeRecordModel : Collection<IDModeRecord>
    {
        public String getModeName(String mode)
        {
            foreach (IDModeRecord modeName in this.Items)
            {
                if (modeName.Mode == mode)
                {
                    return modeName.Name;
                }
            }
            return "";
        }        
    }
    public class QAStatusReportModel : Collection<QAStatusRecord>
    {
        public QAStatusRecord GetQAStatusRecord(String id)
        {
            foreach (QAStatusRecord item in this.Items)
            {
                if (item.ID == id)
                {
                    return item;
                }
            }
            return null;
        }

        public List<String> GetDistinctModes()
        {
            List<String> modeList = new List<string>();
            foreach (QAStatusRecord item in this.Items)
            {
                if (!modeList.Contains(item.Mode))
                {
                    modeList.Add(item.Mode);
                }
            }
            modeList.Sort();
            return modeList;
        }

        public QAStatusReportModel GetQAStatusBasedonMode(String mode)
        {
            QAStatusReportModel records = new QAStatusReportModel();
            foreach (QAStatusRecord item in this.Items)
            {
                if (item.Mode == mode)
                {
                    records.Add(item);
                }
            }
            return records;
        }
    }
    public class QAStatusRecord
    {
        private String m_Mode = String.Empty;
        public String Mode
        {
            get { return m_Mode; }
            set { m_Mode = value; }
        }
        private String m_ID = String.Empty;
        public String ID
        {
            get { return m_ID; }
            set { m_ID = value; }
        }     
        private String m_Status = String.Empty;
        public String Status
        {
            get { return m_Status; }
            set { m_Status = value; }
        }
        private List<TNInfo> m_TNInfo = new List<TNInfo>();
        public List<TNInfo> TNInfo
        {
            get { return m_TNInfo; }
            set { m_TNInfo = value; }
        }
    }
    public class TNInfo
    {
        private String m_TN = String.Empty;
        public String TN
        {
            get { return m_TN; }
            set { m_TN = value; }
        }
        private String m_Status = String.Empty;
        public String Status
        {
            get { return m_Status; }
            set { m_Status = value; }
        }
    }
    public class IDByRegionReportModel : Collection<IDByRegionReportRecord>
    {
        public IList<String> GetDistinctModes()
        {
            List<String> distinctModes = new List<String>();
            foreach (IDByRegionReportRecord record in this.Items)
            {
                if (!distinctModes.Contains(record.Mode))
                {
                    distinctModes.Add(record.Mode);
                }
            }
            distinctModes.Sort();
            return (IList<String>)distinctModes;
        }
        private IList<IDInfo> GetSortedIDs(List<IDInfo> idList, IdSetupCodeInfo setupcodeInfoParams)
        {
            if (idList.Count > 0)
            {
                //sort the id list
                IDComparer comparer = null;
                if (setupcodeInfoParams.SortType == ReportSortBy.ModelCount)
                    comparer = new IDComparer(ReportSortBy.ModelCount);
                else if (setupcodeInfoParams.SortType == ReportSortBy.POS)
                    comparer = new IDComparer(ReportSortBy.POS);
                else if (setupcodeInfoParams.SortType == ReportSortBy.SearchStatistics)
                    comparer = new IDComparer(ReportSortBy.SearchStatistics);
                else
                    comparer = new IDComparer(ReportSortBy.IDNumber);
               
                idList.Sort(comparer);           
            }
            return idList;
        }
        public IList<IDInfo> GetIDs(string mode, IdSetupCodeInfo setupcodeInfoParams)
        {
            List<IDInfo> idList = new List<IDInfo>();
            foreach (IDByRegionReportRecord record in this.Items)
            {
                if (record.Mode == mode)
                {
                    IDInfo id = new IDInfo();
                    id.ID = record.ID;
                    id.ModelCount = record.ModelCount;
                    id.TotalUnitsSold = record.TotalUnitsSold;
                    id.SearchStatisticsCount = record.SearchStatisticsCount;
                    idList.Add(id);
                    id = null;
                }
            }
            return GetSortedIDs(idList,setupcodeInfoParams);
        }
    }
    public class IDByRegionReportRecord
    {
        private String m_mode = String.Empty;
        public String Mode { get { return this.m_mode; } set { this.m_mode = value; } }
        private String m_id = String.Empty;
        public String ID
        {
            get { return m_id; }
            set
            {
                m_id = value; 
                if (!String.IsNullOrEmpty(value))
                {
                    this.m_mode = value.Substring(0, 1);
                }
            }
        }
        private int modelCount = 0;
        public int ModelCount
        {
            get { return modelCount; }
            set { modelCount = value; }
        }
        private int totalUnitssold = 0;
        public int TotalUnitsSold
        {
            get { return totalUnitssold; }
            set { totalUnitssold = value; }
        }
        private int searchstatisticsCount = 0;
        public int SearchStatisticsCount
        {
            get { return searchstatisticsCount; }
            set { searchstatisticsCount = value; }
        }
    }
    public class DeviceCategoryRecords : Collection<DeviceCategory>
    {
        public List<Int32> GetDistinctWorksheetNumbers()
        {
            List<Int32> worksheetNumbers = new List<Int32>();
            foreach (DeviceCategory category in this.Items)
            {
                if (!worksheetNumbers.Contains(category.WorkSheet))
                    worksheetNumbers.Add(category.WorkSheet);

            }

            return worksheetNumbers;
        }
        public List<String> GetModeListUnder(int worksheetNumber)
        {
            List<string> modeList = new List<string>();
            foreach (DeviceCategory category in this.Items)
            {
                if(category.WorkSheet == worksheetNumber)
                    modeList.Add(category.Modes);
            }
            
            return modeList;
        }     
        public string GetMappingName(int worksheetNumber,out bool idMergeFlag)
        {
            StringBuilder defaultName = new StringBuilder();
            idMergeFlag = true;

            foreach (DeviceCategory category in this.Items)
            {
                if (worksheetNumber == category.WorkSheet)
                {
                    if (String.IsNullOrEmpty(category.Mapping))
                    {
                        defaultName.Append(category.Modes + ",");
                        idMergeFlag = false;
                    }
                    else
                    {
                        return category.Mapping;
                    }
                }
            }
            
            string mappName = defaultName.ToString();

            return mappName.Remove(mappName.LastIndexOf(','));//remove the trailing comma(',')
        }
        public string GetMappingName(int worksheetNumber)
        {
            StringBuilder defaultName = new StringBuilder();
           
            foreach (DeviceCategory category in this.Items)
            {
                if (worksheetNumber == category.WorkSheet)
                {
                    if (String.IsNullOrEmpty(category.Mapping))
                    {
                        defaultName.Append(category.Modes + ",");
                        
                    }
                    else
                    {
                        return category.Mapping;
                    }
                }
            }

            string mappName = defaultName.ToString();

            return mappName.Remove(mappName.LastIndexOf(','));//remove the trailing comma(',')
        }
        public DeviceCategoryRecords SetMappingandWorkSheet(String mode, String mappingName, String wsNo)
        {
            foreach (DeviceCategory category in this.Items)
            {
                if (category.Modes == mode)
                {
                    category.Mapping = mappingName;
                    if(wsNo != String.Empty)
                    category.WorkSheet = Int32.Parse(wsNo);
                }
            }
            return this;
        }
        public Boolean Validate(String mappingName, String worksheetNo)
        {
            Int32 count = 0;
            foreach (DeviceCategory category in this.Items)
            {
                if (category.Mapping.ToUpper() == mappingName.ToUpper() && category.WorkSheet != Int32.Parse(worksheetNo))
                {
                    ++count;
                }
            }
            if (count > 0)
                return true;
            else
                return false;
        }
        public Boolean CheckMappingName(Int32 worksheetNo)
        {
            Int32 count = 0;
            foreach (DeviceCategory category in this.Items)
            {
                if (category.Mapping != String.Empty && category.WorkSheet == worksheetNo)
                {
                    ++count;
                }
            }
            if (count > 0)
                return true;
            else
                return false;
        }
        public DeviceCategory GetModeRecord(String mode)
        {            
            foreach (DeviceCategory category in this.Items)
            {
                if (category.Modes == mode)
                    return category;
            }
            return null;
        }
    }
    public class DeviceCategory
    {
        private String modes = String.Empty;
        public String Modes
        {
            get { return modes; }
            set { modes = value; }
        }
        private String mappingName = String.Empty;
        public String Mapping
        {
            get { return mappingName; }
            set { mappingName = value; }
        }
        private Int32 workSheet = 1;
        public Int32 WorkSheet
        {
            get { return workSheet; }
            set { workSheet = value; }
        }
        private IList<String> subDevices = new List<String>();
        public IList<String> SubDevices
        {
            get { return subDevices; }
            set { subDevices = value; }
        }

    }
    public class IDBSCReportModel : Collection<IDBSCReportRecords>
    {
        public IList<String> GetDistinctModes()
        {
            List<String> distinctModes = new List<String>();
            foreach (IDBSCReportRecords record in this.Items)
            {
                if (!distinctModes.Contains(record.Mode))
                {
                    distinctModes.Add(record.Mode);
                }
            }
            distinctModes.Sort();
            return (IList<String>)distinctModes;
        }        
        public void Add(IDBSCReportModel reportModel)
        {
            if (reportModel != null)
            {
                foreach (IDBSCReportRecords record in reportModel.Items)
                {
                    this.Items.Add(record);
                }
            }
        }
        public IDBSCReportModel GetBrandIDModel(String mode, String deviceTypeFlag)
        {
            IDBSCReportModel model = new IDBSCReportModel();
            foreach (IDBSCReportRecords record in this.Items)
            {
                if (record.Mode == mode && record.DeviceTypeFlag == deviceTypeFlag)
                {
                    model.Add(record);
                }
            }
            return model;
        }
        public IDBSCReportModel GetBrandIDModel(List<String> modes, String deviceTypeFlag)
        {
            IDBSCReportModel reportModel = new IDBSCReportModel();
            foreach (String mode in modes)
            {
                IDBSCReportModel subModel = GetBrandIDModel(mode, deviceTypeFlag);
                foreach (IDBSCReportRecords subRecord in subModel.Items)
                {
                    //check whether a record exists with same brand name
                    //IDBSCReportRecords reportModelRecord = reportModel.GetBrandIDRecords(subRecord.Brand);
                    reportModel.Add(subRecord);
                }
            }
            return reportModel;
        }
        public IDBSCReportRecords GetBrandIDRecords(String brand, String deviceTypeFlag, String mode)
        {
            foreach (IDBSCReportRecords record in this.Items)
            {
                if (record.Brand == brand && record.Mode == mode && record.DeviceTypeFlag == deviceTypeFlag)
                {
                    return record;
                }
            }
            return null;
        }              
        public IDBSCReportRecords GetBrandIDRecords(string brand)
        {
            foreach (IDBSCReportRecords record in this.Items)
            {
                if (record.Brand == brand)
                    return record;
            }

            return null;
        }
        public IList<String> GetDistinctSubDeviceTypes(String mode)
        {
            IList<String> subdevicetypeList = new List<String>();
            IDBSCReportModel reportModel = GetBrandIDModel(mode, "Y");
            foreach (IDBSCReportRecords record in reportModel)
            {                
                foreach (DeviceInfo deviceInfo in record.DeviceList)
                {
                    if (!subdevicetypeList.Contains(deviceInfo.RegionalName))
                        subdevicetypeList.Add(deviceInfo.RegionalName);
                }
            }
            return subdevicetypeList;
        }       
        public IDBSCReportModel GetBSCModel(String mode, String dev, String deviceTypeFlag)
        {
            IDBSCReportModel model = new IDBSCReportModel();
            IDBSCReportModel subModel = GetBrandIDModel(mode, deviceTypeFlag);      
            foreach (IDBSCReportRecords record in subModel.Items)
            {
                foreach (DeviceInfo deviceInfo in record.DeviceList)
                {
                    if (deviceInfo.RegionalName == dev)
                    {
                        IDBSCReportRecords existingRecord = model.GetBrandIDRecords(record.Brand);
                        if (existingRecord == null)
                        {
                            //add new
                            existingRecord = new IDBSCReportRecords();
                            existingRecord.Brand = record.Brand;
                            existingRecord.BrandNumber = record.BrandNumber;
                            existingRecord.Mode = record.Mode;
                            existingRecord.DeviceList.Add(deviceInfo);
                            model.Add(existingRecord);
                        }
                        else
                        {
                            existingRecord.DeviceList.Add(deviceInfo);
                        }
                    }
                }               
            }
            return model;
        }
    }
    public class IDBSCReportRecords
    {
        private String mode = String.Empty;
        public String Mode
        {
            get { return mode; }
            set { mode = value; }
        }
        private String name = String.Empty;
        public String Brand
        {
            get { return name; }
            set { name = value; ;}
        }
        private Int32 brandNumber;
        public Int32 BrandNumber
        {
            get { return brandNumber; }
            set { brandNumber = value; ;}
        }
        private String deviceTypeFlag = "N";
        public String DeviceTypeFlag
        {
            get { return deviceTypeFlag; }
            set { deviceTypeFlag = value; }
        }
        private IList<DeviceInfo> _deviceList = new List<DeviceInfo>();
        public IList<DeviceInfo> DeviceList
        {
            get { return _deviceList; }
            set { _deviceList = value; }
        }
        public List<IDInfo> GetBSCIDList(String brand)
        {
            List<IDInfo> idList = new List<IDInfo>();
            foreach (DeviceInfo deviceInfo in this.DeviceList)
            {
                foreach (IDInfo idInfo in deviceInfo.IdList)
                {
                    IDInfo checkexistingId = CheckIdInfoExists(idList, idInfo.ID);
                    if (checkexistingId == null)
                        idList.Add(idInfo);
                    else
                        checkexistingId.ModelCount = checkexistingId.ModelCount + idInfo.ModelCount;
                }
            }
            return idList;
        }
        private IDInfo CheckIdInfoExists(List<IDInfo> idList,String id)
        {
            foreach (IDInfo idInfo in idList)
            {
                if (idInfo.ID == id)
                    return idInfo;
            }
            return null;
        }
    }
    public class IdBrandReportModel
    {
        private SortedList<string, List<IDBrandReportRecord>> items = new SortedList<string, List<IDBrandReportRecord>>();

       
        public IList<string> Modes
        {
            get { return this.items.Keys; }
            
        }

        public void Add(IDBrandReportRecord record)
        {
            if (record != null)
            {
                if (this.items.Keys.Contains(record.Mode))
                {
                    List<IDBrandReportRecord> recordList = items[record.Mode];
                    recordList.Add(record);
                }
                else
                {
                    List<IDBrandReportRecord> recordList = new List<IDBrandReportRecord>();
                    recordList.Add(record);

                    this.items.Add(record.Mode, recordList);
                }
            }
        }

        public IDBrandReportRecord GetIDBrandRecord(string id)
        {
            if (!String.IsNullOrEmpty(id) && this.items.Count > 0)
                {
                    string mode = id.Substring(0, 1);
                    if (items.Keys.Contains(mode))
                    {
                        List<IDBrandReportRecord> recordList = items[mode];

                        foreach (IDBrandReportRecord record in recordList)
                        {
                            if (record.ID.Equals(id, StringComparison.OrdinalIgnoreCase))
                                return record;
                        }
                    }
                }
            

            return null;
        }

        public List<IDBrandReportRecord> GetIDBrandDataForDevice(string mode, string deviceType)
        {
            List<IDBrandReportRecord> idBrandDataForDevice = new List<IDBrandReportRecord>();

            if (this.items.Count > 0 && this.items.ContainsKey(mode))
            {
                List<IDBrandReportRecord> recordsByMode = this.items[mode];

                foreach (IDBrandReportRecord record in recordsByMode)
                {
                    foreach (DeviceInfo device in record.DeviceList)
                    {
                        if (device.RegionalName == deviceType)
                        {
                           IDBrandReportRecord refinedRecord = new IDBrandReportRecord();
                           
                            refinedRecord.ID = record.ID;

                            refinedRecord.DeviceList.Add(device);
                            
                            idBrandDataForDevice.Add(refinedRecord);

                            break;
                        }
                    }
                }
                

            }

            return idBrandDataForDevice;
        }
        private Boolean isWithDeviceTypeExists(IList<BrandInfo> brandList)
        {
            //if all ids are with Device Type Flag N don't add to device list
            Boolean isExists = false;
            if (brandList != null)
            {
                foreach (BrandInfo brandInfo in brandList)
                {
                    if (brandInfo.DeviceTypeFlag == "Y")
                    {
                        isExists = true;
                        break;
                    }
                }
            }
            return isExists;
        }
        public List<String> GetDistinctSubDeviceList(string mode)
        {
            List<string> subDeviceList = new List<string>();
            if (this.items.Count > 0 && this.items.ContainsKey(mode))
            {
                List<IDBrandReportRecord> recordsByMode = this.items[mode];

                foreach (IDBrandReportRecord record in recordsByMode)
                {
                    foreach (DeviceInfo device in record.DeviceList)
                    {
                        if (!subDeviceList.Contains(device.RegionalName) && isWithDeviceTypeExists(device.BrandList))
                            subDeviceList.Add(device.RegionalName);
                    }
                }
            }

            return subDeviceList;
        }
        
        public DataSet GetIDBrandReportDataSet(Model.IdSetupCodeInfo setupcodeInfoParams, Model.IDModeRecordModel allModeNames)
        {
            DataSet reportDataSet = null;
            if (this.items.Count > 0)
            {
                reportDataSet = new DataSet();
                DataTable table = reportDataSet.Tables.Add("ID-Brand");
                table.Columns.Add("ID");
                table.Columns.Add("Brands");
                table.Columns.Add("Brand Count");
                table.Columns.Add("Model Count");
                //IList<string> modeList = reportModel.GetDistinctModes();
                foreach (string mode in this.items.Keys)
                {
                    DataRow row = reportDataSet.Tables["ID-Brand"].NewRow();
                    String modeName = allModeNames.getModeName(mode);
                    //dr[0] = mode;
                    row[0] = modeName + " (" + mode + ")";
                    //row[0] = mode;
                    reportDataSet.Tables["ID-Brand"].Rows.Add(row);
                    row = null;

                    //Model.IdBrandReportModel model = reportModel.GetIDBrandModel(mode);

                    List<IDBrandReportRecord> model = this.items[mode];

                    if (model != null)
                    {
                        foreach (Model.IDBrandReportRecord record in model)
                        {
                            List<Model.BrandInfo> brandList = record.GetCompleteBrands("N");
                            if (brandList.Count > 0)
                            {
                                row = reportDataSet.Tables["ID-Brand"].NewRow();
                                row["ID"] = record.ID;
                                //row["Brands"] = GetSortedBrandString(record.GetCompleteBrands(), setupcodeInfoParams);
                                row["Brands"] = GetSortedBrandString(brandList, setupcodeInfoParams);
                                row["Brand Count"] = brandList.Count;
                                row["Model Count"] = GetIDBrandModelCount(brandList);
                                reportDataSet.Tables["ID-Brand"].Rows.Add(row);
                            }
                        }
                    }

                    table.AcceptChanges();

                }
                reportDataSet.AcceptChanges();
            }

            return reportDataSet;
        }

        #region SaveToText
        public DataSet GetIDBrandModeBasedSetForText(Model.IdSetupCodeInfo setupcodeInfoParams, Model.DeviceCategoryRecords deviceCategories, Boolean subdeviceFlag)
        {
            if (subdeviceFlag)
                return GetIDBrandModeBasedSetWithSubdeviceForText(setupcodeInfoParams, deviceCategories);
            else
                return GetIDBrandModeBasedSetWithoutSubdeviceForText(setupcodeInfoParams, deviceCategories);
        }
        
        private DataSet GetIDBrandModeBasedSetWithoutSubdeviceForText(Model.IdSetupCodeInfo setupcodeInfoParams, Model.DeviceCategoryRecords deviceCategories)
        {
            DataSet reportSet = null;
            if (this.items.Count > 0 && setupcodeInfoParams != null && deviceCategories != null)
            {
                reportSet = new DataSet("ID-Brand");
                //get distinct worksheet number
                List<int> worksheetNumList = deviceCategories.GetDistinctWorksheetNumbers();
                foreach (int worksheetNum in worksheetNumList)
                {
                    //create a datatable for each worksheet.
                    DataTable table = reportSet.Tables.Add(deviceCategories.GetMappingName(worksheetNum));
                    table.Columns.Add("ID");
                    table.Columns.Add("Brand Count");
                    table.Columns.Add("Model Count");
                    table.Columns.Add("Brands");

                    List<string> modeList = deviceCategories.GetModeListUnder(worksheetNum);

                    foreach (string mode in modeList)
                    {
                        List<IDBrandReportRecord> refinedModel = this.items[mode];

                        //get all records in current mode and insert to the table
                        foreach (Model.IDBrandReportRecord record in refinedModel)
                        {
                            List<Model.BrandInfo> brandList = record.GetCompleteBrands("N");
                            if (brandList.Count > 0)
                            {
                                //add the rows and data.
                                DataRow dr = table.NewRow();
                                dr["ID"] = record.ID;
                                //dr["Brands"] = GetSortedBrandString(record.GetCompleteBrands(), setupcodeInfoParams);

                                dr["Brands"] = GetSortedBrandString(brandList, setupcodeInfoParams);
                                dr["Brand Count"] = brandList.Count;
                                dr["Model Count"] = GetIDBrandModelCount(brandList);
                                table.Rows.Add(dr);
                                table.AcceptChanges();
                            }
                        }
                    }
                }
                reportSet.AcceptChanges();
            }
            return reportSet;
        }
        
        private DataSet GetIDBrandModeBasedSetWithSubdeviceForText(Model.IdSetupCodeInfo setupcodeInfoParams, Model.DeviceCategoryRecords deviceCategories)
        {
            //create a dataset to return
            DataSet reportSet = null;
            if (this.items.Count > 0 && setupcodeInfoParams != null && deviceCategories != null)
            {
                reportSet = new DataSet("ID-Brand");

                //get all the modes to be added in each worksheet           
                List<int> worksheetNumberList = deviceCategories.GetDistinctWorksheetNumbers();

                foreach (int number in worksheetNumberList)
                {

                    string mappingName = deviceCategories.GetMappingName(number);

                    DataTable table = reportSet.Tables.Add(deviceCategories.GetMappingName(number));
                    table.Columns.Add("ID");
                    table.Columns.Add("Brand Count");
                    table.Columns.Add("Model Count");
                    table.Columns.Add("Brands");

                    List<string> modeList = deviceCategories.GetModeListUnder(number);

                    foreach (string mode in modeList)
                    {
                        //DataRow dr = table.NewRow();
                        //dr[0] = mode;
                        //table.Rows.Add(dr);

                        //Model.IdBrandReportModel refinedModel = reportModel.GetIDBrandModel(mode);

                        foreach (Model.DeviceCategory deviceCategory in deviceCategories)
                        {
                            if (deviceCategory.Modes == mode)
                            {
                                foreach (string deviceType in deviceCategory.SubDevices)
                                {
                                    DataRow dr = table.NewRow();
                                    dr[0] = deviceType;
                                    table.Rows.Add(dr);

                                    //Model.IdBrandReportModel deviceBasedReport = refinedModel.GetIDBrandModelForDevice(deviceType);
                                    List<IDBrandReportRecord> deviceBasedReport = GetIDBrandDataForDevice(mode, deviceType);

                                    foreach (Model.IDBrandReportRecord record in deviceBasedReport)
                                    {
                                        List<Model.BrandInfo> brandList = record.GetCompleteBrands("Y");
                                        if (brandList.Count > 0)
                                        {
                                            dr = table.NewRow();
                                            dr["ID"] = record.ID;
                                            //dr["Brands"] = GetSortedBrandString(record.GetCompleteBrands(), setupcodeInfoParams);

                                            dr["Brands"] = GetSortedBrandString(brandList, setupcodeInfoParams);
                                            dr["Brand Count"] = brandList.Count;
                                            dr["Model Count"] = GetIDBrandModelCount(brandList);
                                            table.Rows.Add(dr);
                                        }
                                    }
                                }
                            }
                        }
                        table.AcceptChanges();
                    }
                }
                reportSet.AcceptChanges();
            }
            return reportSet;
        }
        #endregion

        #region SaveToExcel
        public DataSet GetIDBrandModeBasedSet(Model.IdSetupCodeInfo setupcodeInfoParams, Model.DeviceCategoryRecords deviceCategories, Boolean subdeviceFlag)
        {
            if (subdeviceFlag)
                return GetIDBrandModeBasedSetWithSubdevice(setupcodeInfoParams, deviceCategories);
            else
                return GetIDBrandModeBasedSetWithoutSubdevice(setupcodeInfoParams, deviceCategories);
        }
        private DataSet GetIDBrandModeBasedSetWithoutSubdevice(Model.IdSetupCodeInfo setupcodeInfoParams, Model.DeviceCategoryRecords deviceCategories)
        {
            DataSet reportSet = null;
            if (this.items.Count > 0 && setupcodeInfoParams != null && deviceCategories != null)
            {
                reportSet = new DataSet("ID-Brand");
                //get distinct worksheet number
                List<int> worksheetNumList = deviceCategories.GetDistinctWorksheetNumbers();
                foreach (int worksheetNum in worksheetNumList)
                {
                    //create a datatable for each worksheet.
                    DataTable table = reportSet.Tables.Add(deviceCategories.GetMappingName(worksheetNum));
                    table.Columns.Add("ID");
                    table.Columns.Add("Brands");
                    table.Columns.Add("Brand Count");
                    table.Columns.Add("Model Count");
                    //modes to be included in the worksheet.
                    List<string> modeList = deviceCategories.GetModeListUnder(worksheetNum);

                    foreach (string mode in modeList)//get data under each mode.
                    {
                        List<IDBrandReportRecord> refinedModel = this.items[mode];

                        //get all records in current mode and insert to the table
                        foreach (Model.IDBrandReportRecord record in refinedModel)
                        {
                            List<Model.BrandInfo> brandList = record.GetCompleteBrands("N");
                            if (brandList.Count > 0)
                            {
                                //add the rows and data.
                                DataRow dr = table.NewRow();
                                dr["ID"] = record.ID;
                                //dr["Brands"] = GetSortedBrandString(record.GetCompleteBrands(), setupcodeInfoParams);

                                dr["Brands"] = GetSortedBrandString(brandList, setupcodeInfoParams);
                                dr["Brand Count"] = brandList.Count;
                                dr["Model Count"] = GetIDBrandModelCount(brandList);
                                table.Rows.Add(dr);
                                table.AcceptChanges();
                            }
                        }
                    }
                }
                reportSet.AcceptChanges();
            }
            return reportSet;
        }
        private DataSet GetIDBrandModeBasedSetWithSubdevice(Model.IdSetupCodeInfo setupcodeInfoParams, Model.DeviceCategoryRecords deviceCategories)
        {
            //create a dataset to return
            DataSet reportSet = null;
            if (this.items.Count > 0 && setupcodeInfoParams != null && deviceCategories != null)
            {
                reportSet = new DataSet("ID-Brand");

                //get all the modes to be added in each worksheet           
                List<int> worksheetNumberList = deviceCategories.GetDistinctWorksheetNumbers();

                foreach (int number in worksheetNumberList)
                {

                    string mappingName = deviceCategories.GetMappingName(number);
                    //One dataTable for each worksheet
                    DataTable table = reportSet.Tables.Add(deviceCategories.GetMappingName(number));
                    table.Columns.Add("ID");
                    table.Columns.Add("Brands");
                    table.Columns.Add("Brand Count");
                    table.Columns.Add("Model Count");
                    List<string> modeList = deviceCategories.GetModeListUnder(number);

                    foreach (string mode in modeList)
                    {
                        //DataRow dr = table.NewRow();
                        //dr[0] = mode;
                        //table.Rows.Add(dr);

                        //Model.IdBrandReportModel refinedModel = reportModel.GetIDBrandModel(mode);

                        foreach (Model.DeviceCategory deviceCategory in deviceCategories)
                        {
                            if (deviceCategory.Modes == mode)
                            {
                                foreach (string deviceType in deviceCategory.SubDevices)
                                {
                                    DataRow dr = table.NewRow();
                                    dr[0] = deviceType;
                                    table.Rows.Add(dr);

                                    //Model.IdBrandReportModel deviceBasedReport = refinedModel.GetIDBrandModelForDevice(deviceType);
                                    List<IDBrandReportRecord> deviceBasedReport = GetIDBrandDataForDevice(mode, deviceType);

                                    foreach (Model.IDBrandReportRecord record in deviceBasedReport)
                                    {
                                        List<Model.BrandInfo> brandList = record.GetCompleteBrands("Y");
                                        if (brandList.Count > 0)
                                        {
                                            dr = table.NewRow();
                                            dr["ID"] = record.ID;
                                            //dr["Brands"] = GetSortedBrandString(record.GetCompleteBrands(), setupcodeInfoParams);

                                            dr["Brands"] = GetSortedBrandString(brandList, setupcodeInfoParams);
                                            dr["Brand Count"] = brandList.Count;
                                            dr["Model Count"] = GetIDBrandModelCount(brandList);
                                            table.Rows.Add(dr);
                                        }
                                    }
                                }
                            }
                        }
                        table.AcceptChanges();
                    }
                }
                reportSet.AcceptChanges();
            }
            return reportSet;
        }
        #endregion

        public Int32 GetIDBrandModelCount(List<Model.BrandInfo> brandList)
        {
            Int32 modelCount = 0;
            foreach (Model.BrandInfo item in brandList)
            {
                modelCount += item.ModelCount;
            }
            return modelCount;
        }

        private string GetSortedBrandString(List<Model.BrandInfo> brandList, Model.IdSetupCodeInfo setupcodeInfoParams)
        {
            StringBuilder sb = new StringBuilder();
            if (brandList.Count > 0)
            {
                //sort the brand list
                Model.BrandComparer comparer = null;
                if (setupcodeInfoParams.SortType == Model.ReportSortBy.ModelCount)
                    comparer = new BrandComparer(Model.ReportSortBy.ModelCount);
                else
                    comparer = new BrandComparer(Model.ReportSortBy.AlphabeticalBrandNames);

                brandList.Sort(comparer);



                foreach (Model.BrandInfo brand in brandList)
                {
                    sb.Append(brand.Name);
                    sb.Append(", ");
                }
                sb.Remove(sb.Length - 2, 2);
            }
            return sb.ToString();
        }
    }
    public class IDBrandReportRecord
    {
        private String _Mode;
        public String Mode
        {
            get { return this._Mode; }
        }
        private String _ID;
        public String ID
        {
            get { return _ID; }
            set
            {
                _ID = value;
                if (!String.IsNullOrEmpty(value))
                {
                    this._Mode = value.Substring(0, 1);
                }
            }
        }
        private List<DeviceInfo> _DeviceList = new List<DeviceInfo>();
        public List<DeviceInfo> DeviceList
        {
            get { return _DeviceList; }
            set { _DeviceList = value; }
        }
        public DeviceInfo GetDeviceInfo(string deviceType)
        {
            foreach (DeviceInfo device in this._DeviceList)
            {
                if (device.Name == deviceType)
                    return device;
            }

            return null;
        }
        public List<BrandInfo> GetCompleteBrands(String DeviceTypeFlag)
        {
          
            if (this.DeviceList.Count > 0)
            {
                DeviceInfo allDeviceInfo = new DeviceInfo();
                allDeviceInfo.Name = "All";

                foreach (DeviceInfo device in this.DeviceList)
                {
                    foreach (BrandInfo brand in device.BrandList)
                    {
                        if (brand.DeviceTypeFlag == DeviceTypeFlag)
                        {
                            //check whether the brand exists in allDeviceinfo object.
                            BrandInfo brandInfo = allDeviceInfo.GetBrandInfo(brand.Name);
                            if (brandInfo == null)
                            {
                                brandInfo = new BrandInfo();
                                brandInfo.Name = brand.Name;
                                brandInfo.ModelCount = brand.ModelCount;
                                allDeviceInfo.BrandList.Add(brandInfo);
                            }
                            else
                            {
                                //if brand already exists, update the model count
                                brandInfo.ModelCount = brandInfo.ModelCount + brand.ModelCount;
                            }
                        }
                    }
                }

                return allDeviceInfo.BrandList;
            }

            return new List<BrandInfo>();
        }
        public DeviceInfo GetDeviceInfo(String mainDevice, String subDevice, String component)
        {
            foreach (DeviceInfo device in this._DeviceList)
            {
                if (device.MainDevice == mainDevice && device.SubDevice == subDevice && device.Component == component)
                    return device;
            }
            return null;
        }
    }
    public class ModelInfoRecord
    {
        public const string REMOTEMODELSTR = "(R)";
        public const string TARGETMODELSTR = "(T)";

        public ModelInfoRecord()
        {
        }
        public ModelInfoRecord(string brand,string isTarget,string model,string id,string dataSource,string tnLink)
        {
            this._BrandName = brand;
            this._IsTarget = isTarget;
            this._ModelName = model;
            this._ID = id;
            this._TNLink = tnLink;
            this._DataSource = dataSource;
        }
        private string _BrandName;
        public string Brand
        {
            get { return _BrandName; }
            set { _BrandName = value; }
        }
        private string _AliasBrandName = String.Empty;

        public string AliasBrand
        {
            get { return _AliasBrandName; }
            set { _AliasBrandName = value; }
        }

        private string _AliasType;

        public string AliasType
        {
            get { return _AliasType; }
            set { _AliasType = value; }
        }
	
        private string _ModelName;
        public string Model
        {
            get { return _ModelName; }
            set { _ModelName = value; }
        }
        private string _ID;
        public string ID
        {
            get { return _ID; }
            set { _ID = value; }
        }
        private string _Mode;

        public string Mode
        {
            get { return _Mode; }
            set { _Mode = value; }
        }
	
        private string _TNLink;
        public string TNLink
        {
            get { return _TNLink; }
            set { _TNLink = value; }
        }
        private string _IsTarget;
        public string IsTarget
        {
            get { return _IsTarget; }
            set { _IsTarget = value; }
        }
        private string _DataSource;
        public string DataSource
        {
            get { return _DataSource; }
            set { _DataSource = value; }
        }
        private string country;
        public string Country
        {
            get { return country; }
            set { country = value; }
        }
        private String _SubRegion;

        public String SubRegion
        {
            get { return _SubRegion; }
            set { _SubRegion = value; }
        }

        private string region;
        public string Region
        {
            get { return region; }
            set { region = value; }
        }
        private string comment;
        public string Comment
        {
            get { return comment; }
            set { comment = value; }
        }
        private string deviceType;

        public string DeviceType
        {
            get { return deviceType; }
            set { deviceType = value; }
        }

        private String _MainDevice;

        public String MainDevice
        {
            get { return _MainDevice; }
            set { _MainDevice = value; }
        }

        private String _SubDevice;

        public String SubDevice
        {
            get { return _SubDevice; }
            set { _SubDevice = value; }
        }

        private String _Component;

        public String Component
        {
            get { return _Component; }
            set { _Component = value; }
        }

        private String _ComponentStatus;

        public String ComponentStatus
        {
            get { return _ComponentStatus; }
            set { _ComponentStatus = value; }
        }

        private String _TrustLevel;

        public String TrustLevel
        {
            get { return _TrustLevel; }
            set { _TrustLevel = value; }
        }



    }
    public class ModelInfoRecordCollection:Collection<ModelInfoRecord>
    {
        private List<ModelInfoRecord> GetAllModelInformation(String id, Boolean keepModels)
        {
            List<ModelInfoRecord> modelidList = null;
            if (this.Items != null)
            {
                modelidList = new List<ModelInfoRecord>();
                foreach (ModelInfoRecord m_Model in this.Items)
                {
                    if (m_Model.ID == id)
                    {
                        modelidList.Add(m_Model);
                    }
                }                
            }
            if (keepModels)
            {
                if (modelidList != null)
                {
                    foreach (ModelInfoRecord m_Model in modelidList)
                    {
                        if(this.Items.Contains(m_Model))
                        {
                            this.Items.Remove(m_Model);
                        }
                    }
                }
            }
            return modelidList;
        }
        public DataSet GetModelInfoDataset(ModelInfoSpecific miscFilters, ref IList<String> m_KeepIdList)
        {
            if (this.Items.Count > 0)
            {
                //Identical ID Elimination
                if (miscFilters.IdenticalIdList != null)
                {
                    foreach (Model.AliasId identicalId in miscFilters.IdenticalIdList)
                    {
                        if (identicalId.AliasID.Length > 0)
                        {
                            //Get Model Details of Retain ID
                            List<ModelInfoRecord> m_IDModelInfo = GetAllModelInformation(identicalId.ID, false);

                            String[] idList = identicalId.AliasID.Split(',');                            
                            foreach (String id in idList)
                            {                               
                                //Get Model Details of Alias ID
                                List<ModelInfoRecord> m_AliasIDModelInfo = GetAllModelInformation(id,true);                                
                                //Check if Information is already Exists
                                if (m_AliasIDModelInfo != null && m_IDModelInfo != null)
                                {
                                    foreach (ModelInfoRecord m_Model in m_AliasIDModelInfo)
                                    {
                                        if(!m_IDModelInfo.Contains(m_Model))
                                        {
                                            m_Model.ID = identicalId.ID;
                                            this.Items.Add(m_Model);
                                        }
                                    }
                                }                                
                            }
                        }
                    }
                }

                if (miscFilters.DisplayMode == DisplayFilter.Distinct)
                    return GetDistinctDataReport(miscFilters);

                //create the structure of dataset based on the filters.
                DataSet reportSet = new DataSet("ModelInfo");
                //fill the data.
                DataTable modelInfoTable = AddDataTableForModelInfoReport(reportSet, miscFilters, "ModelInfoReport");
                //toggling between target model and remote model.
                string isTargetFilter = String.Empty;
                switch (miscFilters.TargetModelFilter)
                {
                    case ModelInfoSpecific.IsTargetFilter.ShowRemoteModelOnly:
                        isTargetFilter = ModelInfoRecord.REMOTEMODELSTR;
                        break;
                    case ModelInfoSpecific.IsTargetFilter.ShowTargetModelOnly:
                        isTargetFilter = ModelInfoRecord.TARGETMODELSTR;
                        break;
                }
                
                foreach (ModelInfoRecord record in this.Items)
                {
                    if (m_KeepIdList.Contains(record.ID))
                    {
                        m_KeepIdList.Remove(record.ID);
                    }
                    DataRow dr = modelInfoTable.NewRow();
                    switch (miscFilters.ModelInfoOptions)
                    {
                        case ModelInfoSpecific.ModelReportOptions.ModelBased:
                            ModelInfoRecord selectedRecord = null;
                            if (!String.IsNullOrEmpty(isTargetFilter))
                            {
                                if (record.IsTarget == isTargetFilter)
                                    selectedRecord = record;
                            }
                            else
                                selectedRecord = record;
                            if (selectedRecord != null)
                            {
                                FillModelBasedData(miscFilters, dr, selectedRecord);
                                modelInfoTable.Rows.Add(dr);
                            }
                            break;
                        case ModelInfoSpecific.ModelReportOptions.TNBased:
                           
                            FillTNBasedData(record, dr);
                            modelInfoTable.Rows.Add(dr);
                            break;
                        case ModelInfoSpecific.ModelReportOptions.BrandBased:
                           
                            FillBrandBasedData(record, dr);
                            modelInfoTable.Rows.Add(dr);
                            break;
                        case ModelInfoSpecific.ModelReportOptions.IDBased:
                           
                            FillIDBasedData(record, dr);
                            modelInfoTable.Rows.Add(dr);
                            break;
                      
                    }                    
                }               
                //Get Distinct Records
                if (miscFilters.ColumnSelector != null)
                {
                    if (modelInfoTable != null)
                    {                        
                        String[] columnNames = new String[modelInfoTable.Columns.Count];
                        for (int i = 0; i < modelInfoTable.Columns.Count; i++)
                        {
                            columnNames[i] = modelInfoTable.Columns[i].ColumnName;
                        }
                        modelInfoTable = modelInfoTable.DefaultView.ToTable(true,columnNames);
                        modelInfoTable.AcceptChanges();
                        reportSet.Tables.RemoveAt(0);
                        reportSet.Tables.Add(modelInfoTable);
                    }
                }
                reportSet.AcceptChanges();                                
                return reportSet;
            }
            return null;
        }
        public DataSet GetQuickSetInfoDataSet(ModelInfoSpecific miscFilters, QuickSetInfo quicksetInfo, ref IList<String> m_KeepIdList)
        {
            if (this.Items.Count > 0)
            {
                #region Identical ID Elimination
                if (miscFilters.IdenticalIdList != null)
                {
                    foreach (Model.AliasId identicalId in miscFilters.IdenticalIdList)
                    {
                        if (identicalId.AliasID.Length > 0)
                        {
                            //Get Model Details of Retain ID
                            List<ModelInfoRecord> m_IDModelInfo = GetAllModelInformation(identicalId.ID, false);

                            String[] idList = identicalId.AliasID.Split(',');
                            foreach (String id in idList)
                            {
                                //Get Model Details of Alias ID
                                List<ModelInfoRecord> m_AliasIDModelInfo = GetAllModelInformation(id, true);
                                //Check if Information is already Exists
                                if (m_AliasIDModelInfo != null && m_IDModelInfo != null)
                                {
                                    foreach (ModelInfoRecord m_Model in m_AliasIDModelInfo)
                                    {
                                        if (!m_IDModelInfo.Contains(m_Model))
                                        {
                                            m_Model.ID = identicalId.ID;
                                            this.Items.Add(m_Model);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                #endregion

                //Remove Columns 
                miscFilters.ColumnSelector = new Dictionary<String, Boolean>();
                miscFilters.ColumnSelector.Add("DataSource", false);
                miscFilters.ColumnSelector.Add("Region", false);
                miscFilters.ColumnSelector.Add("SubRegion", false);
                miscFilters.ColumnSelector.Add("Country", false);
                miscFilters.ColumnSelector.Add("MainDevice", false);
                miscFilters.ColumnSelector.Add("SubDevice", true);
                miscFilters.ColumnSelector.Add("Component", false);
                miscFilters.ColumnSelector.Add("ComponentStatus", false);
                miscFilters.ColumnSelector.Add("TrustLevel", false);
                miscFilters.ColumnSelector.Add("Alias Brand", true);
                miscFilters.ColumnSelector.Add("Alias Type", false);                

                //create the structure of dataset based on the filters.
                DataSet reportSet = new DataSet("ModelInfo");
                //fill the data.
                DataTable modelInfoTable = AddDataTableForModelInfoReport(reportSet, miscFilters, "ModelInfoReport");
                if (modelInfoTable.Columns.Contains("TN Link"))
                {
                    modelInfoTable.Columns.Remove("TN Link");
                }
                //toggling between target model and remote model.
                string isTargetFilter = String.Empty;
                switch (miscFilters.TargetModelFilter)
                {
                    case ModelInfoSpecific.IsTargetFilter.ShowRemoteModelOnly:
                        isTargetFilter = ModelInfoRecord.REMOTEMODELSTR;
                        break;
                    case ModelInfoSpecific.IsTargetFilter.ShowTargetModelOnly:
                        isTargetFilter = ModelInfoRecord.TARGETMODELSTR;
                        break;
                }

                foreach (ModelInfoRecord record in this.Items)
                {
                    if (m_KeepIdList.Contains(record.ID))
                    {
                        m_KeepIdList.Remove(record.ID);
                    }
                    DataRow dr = null;
                    switch (miscFilters.ModelInfoOptions)
                    {
                        case ModelInfoSpecific.ModelReportOptions.ModelBased:
                            ModelInfoRecord selectedRecord = null;
                            if (!String.IsNullOrEmpty(isTargetFilter))
                            {
                                if (record.IsTarget == isTargetFilter)
                                    selectedRecord = record;
                            }
                            else
                                selectedRecord = record;
                            if (selectedRecord != null)
                            {
                                dr = modelInfoTable.NewRow();
                                FillModelBasedData(miscFilters, dr, selectedRecord);
                                modelInfoTable.Rows.Add(dr);
                                dr = null;
                             
                                if (!String.IsNullOrEmpty(selectedRecord.AliasBrand))
                                {
                                    if (selectedRecord.AliasBrand.Contains(","))
                                    {
                                        String[] aliasBrands = selectedRecord.AliasBrand.Split(',');
                                        foreach (String item in aliasBrands)
                                        {
                                            if (!String.IsNullOrEmpty(item))
                                            {
                                                dr = modelInfoTable.NewRow();
                                                FillModelBasedData(miscFilters, dr, selectedRecord);

                                                dr["Brand"] = item;
                                                modelInfoTable.Rows.Add(dr);
                                                dr = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        dr = modelInfoTable.NewRow();
                                        FillModelBasedData(miscFilters, dr, selectedRecord);

                                        dr["Brand"] = selectedRecord.AliasBrand;
                                        modelInfoTable.Rows.Add(dr);
                                        dr = null;
                                    }
                                }
                            }                            
                            break;
                        case ModelInfoSpecific.ModelReportOptions.TNBased:

                            FillTNBasedData(record, dr);
                            modelInfoTable.Rows.Add(dr);
                            break;
                        case ModelInfoSpecific.ModelReportOptions.BrandBased:

                            FillBrandBasedData(record, dr);
                            modelInfoTable.Rows.Add(dr);
                            break;
                        case ModelInfoSpecific.ModelReportOptions.IDBased:

                            FillIDBasedData(record, dr);
                            modelInfoTable.Rows.Add(dr);
                            break;

                    }
                }
                //Get Distinct Records
                if (miscFilters.ColumnSelector != null)
                {
                    if (modelInfoTable != null)
                    {
                        String[] columnNames = new String[modelInfoTable.Columns.Count];
                        for (int i = 0; i < modelInfoTable.Columns.Count; i++)
                        {
                            columnNames[i] = modelInfoTable.Columns[i].ColumnName;
                        }
                        modelInfoTable = modelInfoTable.DefaultView.ToTable(true, columnNames);

                        if (modelInfoTable.Columns.Contains("Alias Brand"))
                            modelInfoTable.Columns.Remove("Alias Brand");
                        if (modelInfoTable.Columns.Contains("Alias Type"))
                            modelInfoTable.Columns.Remove("Alias Type");

                        modelInfoTable.AcceptChanges();
                        reportSet.Tables.RemoveAt(0);
                        reportSet.Tables.Add(modelInfoTable);
                    }
                }
                reportSet.AcceptChanges();

                //Generate BSC Report from Model Info Report
                #region BSC
                //String strExp   = String.Empty;
                //String strSort  = String.Empty; 
                //if (modelInfoTable.Rows.Count > 0)
                //{                    
                //    QuickSetReportModel quicksetModel = new QuickSetReportModel();
                //    DataTable dtUniqBrandId = modelInfoTable.DefaultView.ToTable(true, new String[] { "Brand", "SubDevice", "ID" });

                //    foreach (int deviceCode in quicksetInfo.ModeSetup.Keys)
                //    {
                //        QuickSetModeSetup modeSetup = quicksetInfo.ModeSetup[deviceCode];                    
                //        //DeviceList is the selected Modes provided by User for each Device Name
                //        //Get All Brands for each DeviceList
                //        Char[] modes = modeSetup.ModeList.ToUpper().ToCharArray();
                //        String strMode = String.Empty;
                //        foreach (Char mode in modes)
                //        {
                //            strMode += "'" + mode.ToString() + "',";
                //        }
                        
                //        if (!String.IsNullOrEmpty(strMode))
                //            strMode = strMode.Remove(strMode.Length - 1, 1);
                //        strExp = "SUBSTRING(ID,1,1) in ("+ strMode +")";
                //        String strSubDevice = String.Empty;
                //        if (quicksetInfo.ModeSetup[deviceCode].SubDevices.Count > 0)
                //        {                            
                //            foreach (String subdevice in quicksetInfo.ModeSetup[deviceCode].SubDevices)
                //            {
                //                strSubDevice += "'" + subdevice + "',";
                //            }
                //            if (!String.IsNullOrEmpty(strSubDevice))
                //            {
                //                strSubDevice = strSubDevice.Remove(strSubDevice.Length - 1, 1);
                //                strExp += " and SubDevice in (" + strSubDevice + ")";
                //            }                            
                //        }
                //        strSort = "Brand ASC";                        
                //        DataView dvUniqBrand = dtUniqBrandId.DefaultView;
                //        dvUniqBrand.RowFilter = strExp;
                //        dvUniqBrand.Sort = strSort;
                //        DataTable dtUniqRecords = dvUniqBrand.ToTable(true, new String[] { "Brand" });

                //        #region Process Statndard Brand and Alias Brand
                //        foreach (DataRow itemBrand in dtUniqRecords.Rows)
                //        {
                //            //Get All IDS for each DeviceList and Brand
                //            strExp = "Brand like '" + EscapeLikeValue(itemBrand["Brand"].ToString()) + "' and SUBSTRING(ID,1,1) in (" + strMode + ")";

                //            if (!String.IsNullOrEmpty(strSubDevice))
                //                strExp += " and SubDevice in (" + strSubDevice + ")";
                                
                //            strSort = "Brand ASC, ID ASC";

                //            DataView dvUniqIds = dtUniqBrandId.DefaultView;
                //            dvUniqIds.RowFilter = strExp;
                //            dvUniqIds.Sort = strSort;
                //            DataTable dtUniqIds = dvUniqIds.ToTable(true, new String[] { "ID" });
                //            //DataRow[] drUniqIds = modelInfoTable.Select(strExp, strSort);
                //            foreach (DataRow itemId in dtUniqIds.Rows)
                //            {
                //                //Get Unique Models for each DeviceList, Brand and ID
                //                strExp = "ID like '" + itemId["ID"].ToString() + "' and Brand like '" + EscapeLikeValue(itemBrand["Brand"].ToString()) + "' and SUBSTRING(ID,1,1) in (" + strMode + ")";
                //                if (!String.IsNullOrEmpty(strSubDevice))
                //                    strExp += " and SubDevice in (" + strSubDevice + ")";
                //                strSort = "Model ASC";

                //                DataView dvUniqModels = modelInfoTable.DefaultView;
                //                dvUniqModels.RowFilter = strExp;
                //                dvUniqModels.Sort = strSort;
                //                DataTable dtUniqModels = dvUniqModels.ToTable(true, new String[] { "Model" });
                //                QuickSetReportRecord newrecord = null;
                //                if (quicksetModel.Count > 0)
                //                {
                //                    BrandInfo brand = quicksetModel.GetBrandInfo(modeSetup.ModeList, modeSetup.DeviceName, itemBrand["Brand"].ToString());
                //                    if (brand != null)
                //                    {
                //                        IDInfo id = IsIDExists(brand.IDList, itemId["ID"].ToString());
                //                        if (id != null)
                //                        {
                //                            id.ModelCount += dtUniqModels.Rows.Count;
                //                        }
                //                        else
                //                        {
                //                            id = new IDInfo();
                //                            id.ID = itemId["ID"].ToString();
                //                            id.ModelCount = dtUniqModels.Rows.Count;
                //                            brand.IDList.Add(id);
                //                        }
                //                    }
                //                    else
                //                    {
                //                        newrecord = new QuickSetReportRecord();
                //                        newrecord.DeviceCode = modeSetup.ModeList;
                //                        newrecord.DeviceName = modeSetup.DeviceName;
                //                        newrecord.DeviceNo = deviceCode.ToString();
                //                        brand = new BrandInfo();
                //                        brand.Name = itemBrand["Brand"].ToString();
                //                        IDInfo id = new IDInfo();
                //                        id.ID = itemId["ID"].ToString();
                //                        id.ModelCount = dtUniqModels.Rows.Count;
                //                        brand.IDList.Add(id);
                //                        newrecord.BrandList.Add(brand);
                //                        quicksetModel.Add(newrecord);
                //                        newrecord = null;
                //                    }
                //                }
                //                else
                //                {
                //                    newrecord = new QuickSetReportRecord();
                //                    newrecord.DeviceCode = modeSetup.ModeList;
                //                    newrecord.DeviceName = modeSetup.DeviceName;
                //                    newrecord.DeviceNo = deviceCode.ToString();
                //                    BrandInfo brand = new BrandInfo();
                //                    brand.Name = itemBrand["Brand"].ToString();
                //                    IDInfo id = new IDInfo();
                //                    id.ID = itemId["ID"].ToString();
                //                    id.ModelCount = dtUniqModels.Rows.Count;
                //                    brand.IDList.Add(id);
                //                    newrecord.BrandList.Add(brand);
                //                    quicksetModel.Add(newrecord);
                //                    newrecord = null;
                //                }
                //            }
                //        }
                //        #endregion                        

                //    }
                //    DataTable bscInfoTable = new DataTable("BSCInfoReport");
                //    bscInfoTable.Columns.Add("DeviceNo");
                //    bscInfoTable.Columns.Add("DeviceCode");
                //    bscInfoTable.Columns.Add("DeviceName");
                //    bscInfoTable.Columns.Add("Brand");
                //    bscInfoTable.Columns.Add("ID");
                //    bscInfoTable.AcceptChanges();
                //    DataRow dr = null;
                //    if (quicksetModel.Count > 0)
                //    {
                //        foreach (QuickSetReportRecord record in quicksetModel)
                //        {                            
                //            foreach (BrandInfo brand in record.BrandList)
                //            {
                //                IDComparer comparer = new IDComparer(ReportSortBy.ModelCount);
                //                brand.IDList.Sort(comparer);
                //                List<String> newIdList = new List<String>();
                //                //Setup Priority ID List
                //                if (quicksetInfo.PriorityIDList != null && brand.IDList != null)
                //                {
                //                    String strDeviceCodeandBrand = String.Empty;
                //                    strDeviceCodeandBrand = record.DeviceCode.Trim().ToUpper() + ',' + brand.Name.Trim();
                //                    if (quicksetInfo.PriorityIDList.ContainsKey(strDeviceCodeandBrand))
                //                    {
                //                        List<String> priorityIds = quicksetInfo.PriorityIDList[strDeviceCodeandBrand];
                //                        foreach (String item in priorityIds)
                //                        {
                //                            if (IsIDExists(brand.IDList, item) != null)
                //                            {
                //                                newIdList.Add(item);
                //                            }
                //                        }
                //                        foreach (IDInfo id in brand.IDList)
                //                        {
                //                            if (!newIdList.Contains(id.ID))
                //                            {
                //                                newIdList.Add(id.ID);
                //                            }
                //                        }
                //                    }
                //                }
                //                String strID = String.Empty;
                //                if (newIdList.Count > 0)
                //                {
                //                    foreach (String id in newIdList)
                //                    {
                //                        strID += id + ",";
                //                    }
                //                }
                //                else
                //                {
                //                    foreach (IDInfo id in brand.IDList)
                //                    {
                //                        strID += id.ID + ",";
                //                    }
                //                }
                //                if (!String.IsNullOrEmpty(strID))
                //                    strID = strID.Remove(strID.Length - 1, 1);

                //                dr = bscInfoTable.NewRow();
                //                dr[0] = record.DeviceNo;
                //                dr[1] = record.DeviceCode;
                //                dr[2] = record.DeviceName;
                //                dr[3] = brand.Name;
                //                dr[4] = strID;
                //                bscInfoTable.Rows.Add(dr);
                //                dr = null;
                //            }
                //        }
                //    }
                //    bscInfoTable.AcceptChanges();
                //    if (reportSet.Tables.Contains("BSCInfoReport"))
                //        reportSet.Tables.Remove("BSCInfoReport");
                //    reportSet.Tables.Add(bscInfoTable);
                //}
                #endregion

                return reportSet;
            }
            return null;
        }
        public DataSet ExportQuickSetReport(ModelInfoSpecific miscFilters, QuickSetInfo quicksetInfo)
        {
            if (this.Items.Count > 0)
            {
                //Remove Columns 
                miscFilters.ColumnSelector = new Dictionary<String, Boolean>();
                miscFilters.ColumnSelector.Add("DataSource", false);
                miscFilters.ColumnSelector.Add("Region", false);
                miscFilters.ColumnSelector.Add("SubRegion", false);
                miscFilters.ColumnSelector.Add("Country", false);
                miscFilters.ColumnSelector.Add("MainDevice", false);
                miscFilters.ColumnSelector.Add("SubDevice", true);
                miscFilters.ColumnSelector.Add("Component", false);
                miscFilters.ColumnSelector.Add("ComponentStatus", false);
                miscFilters.ColumnSelector.Add("TrustLevel", false);
                miscFilters.ColumnSelector.Add("Alias Brand", true);
                miscFilters.ColumnSelector.Add("Alias Type", false);

                DataSet reportSet = new DataSet("ModelInfo");
                DataTable modelInfoTable = null;
                DataTable modelInfoTable_Alias = null;
                
                
                modelInfoTable = AddDataTableForModelInfoReport(reportSet, miscFilters, "ModelInfoReport");
                if (modelInfoTable.Columns.Contains("TN Link"))
                {
                    modelInfoTable.Columns.Remove("TN Link");
                }
                modelInfoTable_Alias = AddDataTableForModelInfoReport(reportSet, miscFilters, "ModelInfoReport_Alias");
                if (modelInfoTable_Alias.Columns.Contains("TN Link"))
                {
                    modelInfoTable_Alias.Columns.Remove("TN Link");
                }
                //toggling between target model and remote model.
                string isTargetFilter = String.Empty;
                switch (miscFilters.TargetModelFilter)
                {
                    case ModelInfoSpecific.IsTargetFilter.ShowRemoteModelOnly:
                        isTargetFilter = ModelInfoRecord.REMOTEMODELSTR;
                        break;
                    case ModelInfoSpecific.IsTargetFilter.ShowTargetModelOnly:
                        isTargetFilter = ModelInfoRecord.TARGETMODELSTR;
                        break;
                }

                foreach (ModelInfoRecord record in this.Items)
                {
                    DataRow dr = null;
                    switch (miscFilters.ModelInfoOptions)
                    {
                        case ModelInfoSpecific.ModelReportOptions.ModelBased:
                            ModelInfoRecord selectedRecord = null;
                            if (!String.IsNullOrEmpty(isTargetFilter))
                            {
                                if (record.IsTarget == isTargetFilter)
                                    selectedRecord = record;
                            }
                            else
                                selectedRecord = record;

                            if (selectedRecord != null)
                            {
                                dr = modelInfoTable.NewRow();
                                FillModelBasedData(miscFilters, dr, selectedRecord);
                                modelInfoTable.Rows.Add(dr);

                                DataRow dr_Alias = modelInfoTable_Alias.NewRow();
                                FillModelBasedData(miscFilters, dr_Alias, selectedRecord);
                                modelInfoTable_Alias.Rows.Add(dr_Alias);
                                dr_Alias = null;

                                if (!String.IsNullOrEmpty(selectedRecord.AliasBrand))
                                {
                                    if (selectedRecord.AliasBrand.Contains(","))
                                    {
                                        String[] aliasBrands = selectedRecord.AliasBrand.Split(',');
                                        foreach (String item in aliasBrands)
                                        {
                                            if (!String.IsNullOrEmpty(item))
                                            {
                                                dr_Alias = modelInfoTable_Alias.NewRow();
                                                FillModelBasedData(miscFilters, dr_Alias, selectedRecord);

                                                dr_Alias["Brand"] = item;
                                                modelInfoTable_Alias.Rows.Add(dr_Alias);
                                                dr_Alias = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        dr_Alias = modelInfoTable_Alias.NewRow();
                                        FillModelBasedData(miscFilters, dr_Alias, selectedRecord);

                                        dr_Alias["Brand"] = selectedRecord.AliasBrand;
                                        modelInfoTable_Alias.Rows.Add(dr_Alias);
                                        dr_Alias = null;
                                    }
                                }
                            }
                            break;
                        case ModelInfoSpecific.ModelReportOptions.TNBased:
                            FillTNBasedData(record, dr);
                            modelInfoTable.Rows.Add(dr);
                            break;
                        case ModelInfoSpecific.ModelReportOptions.BrandBased:
                            FillBrandBasedData(record, dr);
                            modelInfoTable.Rows.Add(dr);
                            break;
                        case ModelInfoSpecific.ModelReportOptions.IDBased:
                            FillIDBasedData(record, dr);
                            modelInfoTable.Rows.Add(dr);
                            break;
                    }
                }
                //Get Distinct Records
                if (miscFilters.ColumnSelector != null)
                {
                    if (modelInfoTable != null)
                    {
                        String[] columnNames = new String[modelInfoTable.Columns.Count];
                        for (int i = 0; i < modelInfoTable.Columns.Count; i++)
                        {
                            columnNames[i] = modelInfoTable.Columns[i].ColumnName;
                        }
                        modelInfoTable = modelInfoTable.DefaultView.ToTable(true, columnNames);
                        modelInfoTable.AcceptChanges();
                        reportSet.Tables.RemoveAt(0);
                        reportSet.Tables.Add(modelInfoTable);
                    }
                    if (modelInfoTable_Alias != null)
                    {
                        String[] columnNames = new String[modelInfoTable_Alias.Columns.Count];
                        for (int i = 0; i < modelInfoTable_Alias.Columns.Count; i++)
                        {
                            columnNames[i] = modelInfoTable_Alias.Columns[i].ColumnName;
                        }

                        modelInfoTable_Alias = modelInfoTable_Alias.DefaultView.ToTable(true, columnNames);

                        if (modelInfoTable_Alias.Columns.Contains("Alias Brand"))
                            modelInfoTable_Alias.Columns.Remove("Alias Brand");
                        if (modelInfoTable_Alias.Columns.Contains("Alias Type"))
                            modelInfoTable_Alias.Columns.Remove("Alias Type");

                        modelInfoTable_Alias.AcceptChanges();
                        if (reportSet.Tables.Contains(modelInfoTable_Alias.TableName))
                        {
                            reportSet.Tables.Remove(modelInfoTable_Alias.TableName);
                            reportSet.Tables.Add(modelInfoTable_Alias);
                        }
                    }
                }
                //Generate BSC Report from Model Info Report
                #region BSC
                //if (modelInfoTable_Alias.Rows.Count > 0)
                //{
                //    QuickSetReportModel quicksetModel = new QuickSetReportModel();
                //    DataTable dtUniqBrandId = modelInfoTable_Alias.DefaultView.ToTable(true, new String[] { "Brand", "SubDevice", "ID" });

                //    foreach (Int32 deviceCode in quicksetInfo.ModeSetup.Keys)
                //    {
                //        QuickSetModeSetup modeSetup = quicksetInfo.ModeSetup[deviceCode];                   
                //        //DeviceList is the selected Modes provided by User for each Device Name
                //        //Get All Brands for each DeviceList
                //        Char[] modes = modeSetup.ModeList.ToUpper().ToCharArray();
                //        String strMode = String.Empty;
                //        foreach (Char mode in modes)
                //        {
                //            strMode += "'" + mode.ToString() + "',";
                //        }
                //        if (!String.IsNullOrEmpty(strMode))
                //            strMode = strMode.Remove(strMode.Length - 1, 1);
                //        String strExp = "SUBSTRING(ID,1,1) in (" + strMode + ")";
                //        String strSubDevice = String.Empty;
                //        if (quicksetInfo.ModeSetup[deviceCode].SubDevices.Count > 0)
                //        {
                //            foreach (String subdevice in quicksetInfo.ModeSetup[deviceCode].SubDevices)
                //            {
                //                strSubDevice += "'" + subdevice + "',";
                //            }
                //            if (!String.IsNullOrEmpty(strSubDevice))
                //            {
                //                strSubDevice = strSubDevice.Remove(strSubDevice.Length - 1, 1);
                //                strExp += " and SubDevice in (" + strSubDevice + ")";
                //            }
                //        }
                //        String strSort = "Brand ASC";


                //        DataView dvUniqBrand = dtUniqBrandId.DefaultView;
                //        dvUniqBrand.RowFilter = strExp;
                //        dvUniqBrand.Sort = strSort;
                //        DataTable dtUniqRecords = dvUniqBrand.ToTable(true, new String[] { "Brand" });

                //        #region Process Statndard Brand and Alias Brand
                //        foreach (DataRow itemBrand in dtUniqRecords.Rows)
                //        {
                //            //Get All IDS for each DeviceList and Brand
                //            strExp = "Brand like '" + EscapeLikeValue(itemBrand["Brand"].ToString()) + "' and SUBSTRING(ID,1,1) in (" + strMode + ")";
                //            if (!String.IsNullOrEmpty(strSubDevice))
                //                strExp += " and SubDevice in (" + strSubDevice + ")";
                //            strSort = "Brand ASC, ID ASC";

                //            DataView dvUniqIds = dtUniqBrandId.DefaultView;
                //            dvUniqIds.RowFilter = strExp;
                //            dvUniqIds.Sort = strSort;
                //            DataTable dtUniqIds = dvUniqIds.ToTable(true, new String[] { "ID" });
                //            //DataRow[] drUniqIds = modelInfoTable.Select(strExp, strSort);
                //            foreach (DataRow itemId in dtUniqIds.Rows)
                //            {
                //                //Get Unique Models for each DeviceList, Brand and ID
                //                strExp = "ID like '" + itemId["ID"].ToString() + "' and Brand like '" + EscapeLikeValue(itemBrand["Brand"].ToString()) + "' and SUBSTRING(ID,1,1) in (" + strMode + ")";
                //                if (!String.IsNullOrEmpty(strSubDevice))
                //                    strExp += " and SubDevice in (" + strSubDevice + ")";
                //                strSort = "Model ASC";

                //                DataView dvUniqModels = modelInfoTable_Alias.DefaultView;
                //                dvUniqModels.RowFilter = strExp;
                //                dvUniqModels.Sort = strSort;
                //                DataTable dtUniqModels = dvUniqModels.ToTable(true, new String[] { "Model" });
                //                QuickSetReportRecord newrecord = null;
                //                if (quicksetModel.Count > 0)
                //                {
                //                    BrandInfo brand = quicksetModel.GetBrandInfo(modeSetup.ModeList, modeSetup.DeviceName, itemBrand["Brand"].ToString());
                //                    if (brand != null)
                //                    {
                //                        IDInfo id = IsIDExists(brand.IDList, itemId["ID"].ToString());
                //                        if (id != null)
                //                        {
                //                            id.ModelCount += dtUniqModels.Rows.Count;
                //                        }
                //                        else
                //                        {
                //                            id = new IDInfo();
                //                            id.ID = itemId["ID"].ToString();
                //                            id.ModelCount = dtUniqModels.Rows.Count;
                //                            brand.IDList.Add(id);
                //                        }
                //                    }
                //                    else
                //                    {
                //                        newrecord = new QuickSetReportRecord();
                //                        newrecord.DeviceCode = modeSetup.ModeList;
                //                        newrecord.DeviceName = modeSetup.DeviceName;
                //                        newrecord.DeviceNo = deviceCode.ToString();
                //                        brand = new BrandInfo();
                //                        brand.Name = itemBrand["Brand"].ToString();
                //                        IDInfo id = new IDInfo();
                //                        id.ID = itemId["ID"].ToString();
                //                        id.ModelCount = dtUniqModels.Rows.Count;
                //                        brand.IDList.Add(id);
                //                        newrecord.BrandList.Add(brand);
                //                        quicksetModel.Add(newrecord);
                //                        newrecord = null;
                //                    }
                //                }
                //                else
                //                {
                //                    newrecord = new QuickSetReportRecord();
                //                    newrecord.DeviceCode = modeSetup.ModeList;
                //                    newrecord.DeviceName = modeSetup.DeviceName;
                //                    newrecord.DeviceNo = deviceCode.ToString();
                //                    BrandInfo brand = new BrandInfo();
                //                    brand.Name = itemBrand["Brand"].ToString();
                //                    IDInfo id = new IDInfo();
                //                    id.ID = itemId["ID"].ToString();
                //                    id.ModelCount = dtUniqModels.Rows.Count;
                //                    brand.IDList.Add(id);
                //                    newrecord.BrandList.Add(brand);
                //                    quicksetModel.Add(newrecord);
                //                    newrecord = null;
                //                }
                //            }
                //        }
                //        #endregion                        

                //    }
                //    DataTable bscInfoTable = new DataTable("BSC");
                //    bscInfoTable.Columns.Add("DeviceNo");
                //    bscInfoTable.Columns.Add("DeviceCode");
                //    bscInfoTable.Columns.Add("DeviceName");
                //    bscInfoTable.Columns.Add("Brand");
                //    bscInfoTable.Columns.Add("ID");
                //    bscInfoTable.AcceptChanges();
                //    DataRow dr = null;
                //    if (quicksetModel.Count > 0)
                //    {
                //        foreach (QuickSetReportRecord record in quicksetModel)
                //        {
                //            foreach (BrandInfo brand in record.BrandList)
                //            {
                //                IDComparer comparer = new IDComparer(ReportSortBy.ModelCount);
                //                brand.IDList.Sort(comparer);
                //                List<String> newIdList = new List<String>();
                //                //Setup Priority ID List
                //                if (quicksetInfo.PriorityIDList != null && brand.IDList != null)
                //                {
                //                    String strDeviceCodeandBrand = String.Empty;
                //                    strDeviceCodeandBrand = record.DeviceCode.Trim().ToUpper() + ',' + brand.Name.Trim();
                //                    if (quicksetInfo.PriorityIDList.ContainsKey(strDeviceCodeandBrand))
                //                    {
                //                        List<String> priorityIds = quicksetInfo.PriorityIDList[strDeviceCodeandBrand];
                //                        foreach (String item in priorityIds)
                //                        {
                //                            if (IsIDExists(brand.IDList, item) != null)
                //                            {
                //                                newIdList.Add(item);
                //                            }
                //                        }
                //                        foreach (IDInfo id in brand.IDList)
                //                        {
                //                            if (!newIdList.Contains(id.ID))
                //                            {
                //                                newIdList.Add(id.ID);
                //                            }
                //                        }
                //                    }
                //                }
                //                String strID = String.Empty;
                //                if (newIdList.Count > 0)
                //                {
                //                    foreach (String id in newIdList)
                //                    {
                //                        strID += id + ",";
                //                    }
                //                }
                //                else
                //                {
                //                    foreach (IDInfo id in brand.IDList)
                //                    {
                //                        strID += id.ID + ",";
                //                    }
                //                }

                //                if (!String.IsNullOrEmpty(strID))
                //                    strID = strID.Remove(strID.Length - 1, 1);

                //                dr = bscInfoTable.NewRow();
                //                dr[0] = record.DeviceNo;
                //                dr[1] = record.DeviceCode;
                //                dr[2] = record.DeviceName;
                //                dr[3] = brand.Name;
                //                dr[4] = strID;
                //                bscInfoTable.Rows.Add(dr);
                //                dr = null;
                //            }
                //        }
                //    }
                //    bscInfoTable.AcceptChanges();
                //    if (reportSet.Tables.Contains("BSC"))
                //        reportSet.Tables.Remove("BSC");
                //    reportSet.Tables.Add(bscInfoTable);
                //}
                #endregion

                reportSet.AcceptChanges();
                return reportSet;
            }
            return null;
        }
        private IDInfo IsIDExists(List<IDInfo> idList, String id)
        {
            if (idList.Count > 0)
            {
                foreach (IDInfo item in idList)
                {
                    if (item.ID.ToUpper() == id.ToUpper())
                        return item;
                }
            }
            return null;
        }
        private String EscapeLikeValue(String value)
        {
            StringBuilder sb = new StringBuilder(value.Length);
            for (int i = 0; i < value.Length; i++)
            {
                char c = value[i];
                switch (c)
                {
                    case ']':
                    case '[':
                    case '%':
                    case '*':
                        sb.Append("[").Append(c).Append("]");
                        break;
                    case '\'':
                        sb.Append("''");
                        break;
                    default:
                        sb.Append(c);
                        break;
                }
            }
            return sb.ToString();
        }
        public DataSet ExportModelInfo(ModelInfoSpecific miscFilters)
        {
            if (this.Items.Count > 0)
            {
                if (miscFilters.DisplayMode == DisplayFilter.Distinct)
                    return ExportDistinctDataReport(miscFilters);

                DataSet reportSet = new DataSet("ModelInfo");
                //Check if the number of Rows Exceeds 65536 create new Data Table and send a valide message to user
                Int32 recordCount = 1;
                Int32 worksheetNumber = 1;
                String workSheet = "ModelInfoReport" + "_" + worksheetNumber.ToString();
                DataTable modelInfoTable = null;
                DataTable modelInfoTable_Alias = null;
                if (miscFilters.QuickSetFormat == true)
                {
                    modelInfoTable = AddDataTableForModelInfoReport(reportSet, miscFilters, "ModelInfoReport");
                    modelInfoTable_Alias = AddDataTableForModelInfoReport(reportSet, miscFilters, "ModelInfoReport_Alias");                    
                }
                else
                {
                    if (this.Items.Count > 65536)
                        modelInfoTable = AddDataTableForModelInfoReport(reportSet, miscFilters, workSheet);
                    else
                        modelInfoTable = AddDataTableForModelInfoReport(reportSet, miscFilters, "ModelInfoReport");
                }
                //toggling between target model and remote model.
                string isTargetFilter = String.Empty;
                switch (miscFilters.TargetModelFilter)
                {
                    case ModelInfoSpecific.IsTargetFilter.ShowRemoteModelOnly:
                        isTargetFilter = ModelInfoRecord.REMOTEMODELSTR;
                        break;
                    case ModelInfoSpecific.IsTargetFilter.ShowTargetModelOnly:
                        isTargetFilter = ModelInfoRecord.TARGETMODELSTR;
                        break;
                }

                foreach (ModelInfoRecord record in this.Items)
                {
                    DataRow dr = modelInfoTable.NewRow();                   
                    switch (miscFilters.ModelInfoOptions)
                    {
                        case ModelInfoSpecific.ModelReportOptions.ModelBased:
                            ModelInfoRecord selectedRecord = null;
                            if (!String.IsNullOrEmpty(isTargetFilter))
                            {
                                if (record.IsTarget == isTargetFilter)
                                    selectedRecord = record;
                            }
                            else
                                selectedRecord = record;

                            if (selectedRecord != null)
                            {
                                FillModelBasedData(miscFilters, dr, selectedRecord);
                                modelInfoTable.Rows.Add(dr);

                                if (miscFilters.QuickSetFormat == true)
                                {
                                    DataRow dr_Alias = modelInfoTable_Alias.NewRow();
                                    FillModelBasedData(miscFilters, dr_Alias, selectedRecord);
                                    modelInfoTable_Alias.Rows.Add(dr_Alias);
                                    dr_Alias = null;
                                    if (!String.IsNullOrEmpty(selectedRecord.AliasBrand))
                                    {
                                        if (selectedRecord.AliasBrand.Contains(","))
                                        {
                                            String[] aliasBrands = selectedRecord.AliasBrand.Split(',');                                            
                                            foreach (String item in aliasBrands)
                                            {
                                                if (!String.IsNullOrEmpty(item))
                                                {
                                                    dr_Alias = modelInfoTable_Alias.NewRow();
                                                    FillModelBasedData(miscFilters, dr_Alias, selectedRecord);

                                                    dr_Alias["Brand"] = item;
                                                    modelInfoTable_Alias.Rows.Add(dr_Alias);
                                                    dr_Alias = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            dr_Alias = modelInfoTable_Alias.NewRow();
                                            FillModelBasedData(miscFilters, dr_Alias, selectedRecord);

                                            dr_Alias["Brand"] = selectedRecord.AliasBrand;
                                            modelInfoTable_Alias.Rows.Add(dr_Alias);
                                            dr_Alias = null;
                                        }
                                    }                                              
                                }
                                recordCount++;
                                if (recordCount >= 65536 && miscFilters.QuickSetFormat == false)
                                {
                                    reportSet.AcceptChanges();
                                    recordCount = 1;
                                    worksheetNumber++;
                                    workSheet = "ModelInfoReport" + "_" + worksheetNumber.ToString();
                                    modelInfoTable = AddDataTableForModelInfoReport(reportSet, miscFilters, workSheet);
                                }
                            }
                            break;
                        case ModelInfoSpecific.ModelReportOptions.TNBased:
                            FillTNBasedData(record, dr);
                            modelInfoTable.Rows.Add(dr);
                            recordCount++;
                            if (recordCount >= 65536)
                            {
                                reportSet.AcceptChanges();
                                recordCount = 1;
                                worksheetNumber++;
                                workSheet = "ModelInfoReport" + "_" + worksheetNumber.ToString();
                                modelInfoTable = AddDataTableForModelInfoReport(reportSet, miscFilters, workSheet);

                            }
                            break;
                        case ModelInfoSpecific.ModelReportOptions.BrandBased:
                            FillBrandBasedData(record, dr);
                            modelInfoTable.Rows.Add(dr);
                            recordCount++;
                            if (recordCount >= 65536)
                            {
                                reportSet.AcceptChanges();
                                recordCount = 1;
                                worksheetNumber++;
                                workSheet = "ModelInfoReport" + "_" + worksheetNumber.ToString();
                                modelInfoTable = AddDataTableForModelInfoReport(reportSet, miscFilters, workSheet);

                            }
                            break;
                        case ModelInfoSpecific.ModelReportOptions.IDBased:
                            FillIDBasedData(record, dr);
                            modelInfoTable.Rows.Add(dr);
                            recordCount++;
                            if (recordCount >= 65536)
                            {
                                reportSet.AcceptChanges();
                                recordCount = 1;
                                worksheetNumber++;
                                workSheet = "ModelInfoReport" + "_" + worksheetNumber.ToString();
                                modelInfoTable = AddDataTableForModelInfoReport(reportSet, miscFilters, workSheet);

                            }
                            break;
                    }
                }
                //Get Distinct Records
                if (miscFilters.ColumnSelector != null)
                {
                    if (modelInfoTable != null)
                    {
                        String[] columnNames = new String[modelInfoTable.Columns.Count];
                        for (int i = 0; i < modelInfoTable.Columns.Count; i++)
                        {
                            columnNames[i] = modelInfoTable.Columns[i].ColumnName;
                        }
                        modelInfoTable = modelInfoTable.DefaultView.ToTable(true, columnNames);
                        modelInfoTable.AcceptChanges();
                        reportSet.Tables.RemoveAt(0);
                        reportSet.Tables.Add(modelInfoTable);
                    }
                    if (modelInfoTable_Alias != null)
                    {
                        String[] columnNames = new String[modelInfoTable_Alias.Columns.Count];
                        for (int i = 0; i < modelInfoTable_Alias.Columns.Count; i++)
                        {
                            columnNames[i] = modelInfoTable_Alias.Columns[i].ColumnName;
                        }
                        
                        modelInfoTable_Alias = modelInfoTable_Alias.DefaultView.ToTable(true, columnNames);

                        if (modelInfoTable_Alias.Columns.Contains("Alias Brand"))
                            modelInfoTable_Alias.Columns.Remove("Alias Brand");
                        if (modelInfoTable_Alias.Columns.Contains("Alias Type"))
                            modelInfoTable_Alias.Columns.Remove("Alias Type");
                        modelInfoTable_Alias.AcceptChanges();
                        if (reportSet.Tables.Contains(modelInfoTable_Alias.TableName))
                        {
                            reportSet.Tables.Remove(modelInfoTable_Alias.TableName);
                            reportSet.Tables.Add(modelInfoTable_Alias);
                        }
                    }
                }
                reportSet.AcceptChanges();
                return reportSet;
            }
            return null;
        }
        public DataSet GetSubReport(string selectedVal, ModelInfoSpecific miscFilters)
        {
            if (!String.IsNullOrEmpty(selectedVal))
            {
                DataSet reportSet = new DataSet("SubReport");
                DataTable reportTab = AddDataTableForModelInfoReport(reportSet, miscFilters, "ModelInfoReport");
                DataRow dr = null;
                switch (miscFilters.ModelInfoOptions)
                {
                    case ModelInfoSpecific.ModelReportOptions.ModelBased:
                        foreach (ModelInfoRecord record in this.Items)
                        {
                            if (record.Model == selectedVal)
                            {
                                dr = reportTab.NewRow();
                                FillModelBasedData(miscFilters, dr, record);
                                reportTab.Rows.Add(dr);
                            }
                        }
                        break;
                    case ModelInfoSpecific.ModelReportOptions.TNBased:
                        foreach (ModelInfoRecord record in this.Items)
                        {
                            if (record.TNLink == selectedVal)
                            {
                                dr = reportTab.NewRow();
                                FillTNBasedData(record,dr);
                                reportTab.Rows.Add(dr);
                            }
                        }
                        break;
                    case ModelInfoSpecific.ModelReportOptions.BrandBased:
                        foreach (ModelInfoRecord record in this.Items)
                        {
                            if (record.Brand == selectedVal)
                            {
                                dr = reportTab.NewRow();
                                FillBrandBasedData(record, dr);
                                reportTab.Rows.Add(dr);
                            }
                        }
                        break;
                    case ModelInfoSpecific.ModelReportOptions.IDBased:
                        foreach (ModelInfoRecord record in this.Items)
                        {
                            if (record.ID == selectedVal)
                            {
                                dr = reportTab.NewRow();
                                FillIDBasedData(record, dr);
                                reportTab.Rows.Add(dr);
                            }
                        }
                        break;

                }
                reportTab.Columns.RemoveAt(0);
                reportSet.AcceptChanges();

                return reportSet;
            }

            return null;

        }
        private DataSet GetDistinctDataReport(ModelInfoSpecific miscFilters)
        {
            if (this.Items.Count > 0)
            {
                DataSet reportSet = new DataSet("ModelInfoReport");
                DataTable reportTab = reportSet.Tables.Add("ModelInfoReport");
                List<string> avoidDuplicates = new List<string>();
                DataRow dr = null;

                //toggling between target model and remote model.
                string isTargetFilter = String.Empty;
                switch (miscFilters.TargetModelFilter)
                {
                    case ModelInfoSpecific.IsTargetFilter.ShowRemoteModelOnly:
                        isTargetFilter = ModelInfoRecord.REMOTEMODELSTR;
                        break;
                    case ModelInfoSpecific.IsTargetFilter.ShowTargetModelOnly:
                        isTargetFilter = ModelInfoRecord.TARGETMODELSTR;
                        break;
                }


                switch (miscFilters.ModelInfoOptions)
                {
                    case ModelInfoSpecific.ModelReportOptions.ModelBased:
                        reportTab.Columns.Add("Model");
                        //create data table
                        foreach (ModelInfoRecord record in this.Items)
                        {
                            ModelInfoRecord selectedRecord = null;
                            if (!String.IsNullOrEmpty(isTargetFilter))
                            {
                                if (record.IsTarget == isTargetFilter)
                                    selectedRecord = record;
                            }
                            else
                                selectedRecord = record;

                            if (selectedRecord != null)
                            {
                                if (!avoidDuplicates.Contains(selectedRecord.Model))
                                {
                                    dr = reportTab.NewRow();
                                    dr["Model"] = selectedRecord.Model;
                                    reportTab.Rows.Add(dr);
                                    avoidDuplicates.Add(selectedRecord.Model);
                                }
                            }

                        }
                       
                        break;
                    case ModelInfoSpecific.ModelReportOptions.TNBased:
                        reportTab.Columns.Add("TN");
                        //create data table
                        foreach (ModelInfoRecord record in this.Items)
                        {
                            if (!avoidDuplicates.Contains(record.TNLink))
                            {
                                dr = reportTab.NewRow();
                                dr["TN"] = record.TNLink;
                                reportTab.Rows.Add(dr);
                                avoidDuplicates.Add(record.TNLink);
                            }

                        }
                        break;
                    case ModelInfoSpecific.ModelReportOptions.BrandBased:
                        reportTab.Columns.Add("Brand");
                        reportTab.Columns.Add("Alias Brand");
                        reportTab.Columns.Add("Alias Type");
                        //create data table
                        foreach (ModelInfoRecord record in this.Items)
                        {
                            if (!avoidDuplicates.Contains(record.Brand))
                            {
                                dr = reportTab.NewRow();
                                dr["Brand"] = record.Brand;
                                dr["Alias Brand"] = record.AliasBrand;
                                dr["Alias Type"] = record.AliasType;
                                reportTab.Rows.Add(dr);
                                avoidDuplicates.Add(record.Brand);
                            }

                        }
                        break;
                    case ModelInfoSpecific.ModelReportOptions.IDBased:
                        reportTab.Columns.Add("ID");
                        //create data table
                        foreach (ModelInfoRecord record in this.Items)
                        {
                            if (!avoidDuplicates.Contains(record.ID))
                            {
                                dr = reportTab.NewRow();
                                dr["ID"] = record.ID;
                                reportTab.Rows.Add(dr);
                                avoidDuplicates.Add(record.ID);
                            }

                        }
                        break;
                   
                }

                reportSet.AcceptChanges();

                return reportSet;
            }

            return null;
        }
        private DataSet ExportDistinctDataReport(ModelInfoSpecific miscFilters)
        {
            if (miscFilters != null)
            {
                DataSet reportSet = new DataSet("ModelInfoReport");
                DataSet dsReport = GetDistinctDataReport(miscFilters);
                
                if (dsReport != null && dsReport.Tables.Count > 0)
                {
                    DataTable modelInfoTable = dsReport.Tables[0];
                    DataTable newTable = null;
                    if (modelInfoTable.Rows.Count > 65536)
                    {
                        //split.
                        Int32 recordCount = 1;
                        Int32 worksheetNumber = 1;
                        String workSheet = "ModelInfoReport" + "_" + worksheetNumber.ToString();
                        newTable = modelInfoTable.Clone();
                        newTable.TableName = workSheet;
                        reportSet.Tables.Add(newTable);
                        foreach (DataRow row in modelInfoTable.Rows)
                        {
                            DataRow newRow = newTable.NewRow();
                            newRow[0] = row[0];
                            newTable.Rows.Add(newRow);
                            recordCount++;
                            if (recordCount >= 65536)
                            {
                                reportSet.AcceptChanges();

                                recordCount = 1;
                                worksheetNumber++;
                                workSheet = "ModelInfoReport" + "_" + worksheetNumber.ToString();
                                newTable = modelInfoTable.Clone();
                                newTable.TableName = workSheet;
                                reportSet.Tables.Add(newTable);

                            }
                        }

                        return reportSet;
                    }

                    return dsReport;
                }
            }

            return null;
        }        
        private static void FillIDBasedData(ModelInfoRecord record, DataRow dr)
        {
            dr["ID"] = record.ID;
            //dr["Mode"] = record.Region;
            //dr["DeviceType"] = record.DeviceType;
            dr["MainDevice"] = record.MainDevice;
            dr["SubDevice"] = record.SubDevice;
            dr["Component"] = record.Component;
            dr["ComponentStatus"] = record.ComponentStatus;
            dr["Region"] = record.Region;
            dr["SubRegion"] = record.SubRegion;
            dr["Country"] = record.Country;
            dr["DataSource"] = record.DataSource;
            dr["TrustLevel"] = record.TrustLevel;
            
        }
        private static void FillBrandBasedData(ModelInfoRecord record, DataRow dr)
        {
            dr["Brand"] = record.Brand;
            dr["Alias Brand"] = record.AliasBrand;
            dr["Alias Type"] = record.AliasType;
            dr["Mode"] = record.Mode;
            //dr["DeviceType"] = record.DeviceType;
            dr["MainDevice"] = record.MainDevice;
            dr["SubDevice"] = record.SubDevice;
            dr["Component"] = record.Component;
            dr["ComponentStatus"] = record.ComponentStatus;
            dr["Region"] = record.Region;
            dr["SubRegion"] = record.SubRegion;
            dr["Country"] = record.Country;
            dr["DataSource"] = record.DataSource;
            dr["TrustLevel"] = record.TrustLevel;
            
        }
        private static void FillTNBasedData(ModelInfoRecord record, DataRow dr)
        {
            dr["TN"] = record.TNLink;
            dr["Mode"] = record.Mode;
            //dr["DeviceType"] = record.DeviceType;
            dr["MainDevice"] = record.MainDevice;
            dr["SubDevice"] = record.SubDevice;
            dr["Component"] = record.Component;
            dr["ComponentStatus"] = record.ComponentStatus;
            dr["Region"] = record.Region;
            dr["SubRegion"] = record.SubRegion;
            dr["Country"] = record.Country;
            dr["TrustLevel"] = record.TrustLevel;
            dr["DataSource"] = record.DataSource;
           
        }
        private static void FillModelBasedData(ModelInfoSpecific miscFilters, DataRow dr, ModelInfoRecord selectedRecord)
        {
            if (miscFilters.RowSelector != null)
            {
                String m_Filter = String.Empty;
                foreach (String item in miscFilters.RowSelector.Keys)
                {
                    if (miscFilters.RowSelector[item] == false)
                    {
                        m_Filter += item + ",";
                    }
                }
                if (miscFilters.RowSelectorName == "MainDevice")
                {
                    if (m_Filter.Contains(selectedRecord.MainDevice))
                    {
                        selectedRecord.MainDevice = String.Empty;
                    }
                }
                if (miscFilters.RowSelectorName == "SubDevice")
                {
                    if (m_Filter.Contains(selectedRecord.SubDevice))
                    {
                        selectedRecord.SubDevice = String.Empty;
                    }
                }
            }
            if (miscFilters.ColumnSelector != null)
            {
                dr["Model"] = selectedRecord.Model;
                dr["Brand"] = selectedRecord.Brand;

                if (miscFilters.ColumnSelector.ContainsKey("Alias Brand") == true)
                    if (miscFilters.ColumnSelector["Alias Brand"] == true)
                        dr["Alias Brand"] = selectedRecord.AliasBrand;

                if (miscFilters.ColumnSelector.ContainsKey("Alias Type") == true)
                    if (miscFilters.ColumnSelector["Alias Type"] == true)
                        dr["Alias Type"] = selectedRecord.AliasType;

                dr["T/R"] = selectedRecord.IsTarget;
                dr["ID"] = selectedRecord.ID;
                if (miscFilters.ColumnSelector.ContainsKey("DataSource") == true)
                    if (miscFilters.ColumnSelector["DataSource"] == true)
                        dr["DataSource"] = selectedRecord.DataSource;
                if (miscFilters.SelectTNLink == true)
                {
                    if (miscFilters.ColumnSelector.ContainsKey("TN Link") == true)
                        if (miscFilters.ColumnSelector["TN Link"] == true)
                            dr["TN Link"] = selectedRecord.TNLink;
                }
                if (miscFilters.ColumnSelector.ContainsKey("MainDevice") == true)
                    if (miscFilters.ColumnSelector["MainDevice"] == true)
                        dr["MainDevice"] = selectedRecord.MainDevice;
                if (miscFilters.ColumnSelector.ContainsKey("SubDevice") == true)
                    if (miscFilters.ColumnSelector["SubDevice"] == true)
                        dr["SubDevice"] = selectedRecord.SubDevice;
                if (miscFilters.ColumnSelector.ContainsKey("Component") == true)
                    if (miscFilters.ColumnSelector["Component"] == true)
                        dr["Component"] = selectedRecord.Component;
                if (miscFilters.ColumnSelector.ContainsKey("ComponentStatus") == true)
                    if (miscFilters.ColumnSelector["ComponentStatus"] == true)
                        dr["ComponentStatus"] = selectedRecord.ComponentStatus;
                if (miscFilters.ColumnSelector.ContainsKey("Region") == true)
                    if (miscFilters.ColumnSelector["Region"] == true)
                        dr["Region"] = selectedRecord.Region;
                if (miscFilters.ColumnSelector.ContainsKey("SubRegion") == true)
                    if (miscFilters.ColumnSelector["SubRegion"] == true)
                        dr["SubRegion"] = selectedRecord.SubRegion;
                if (miscFilters.ColumnSelector.ContainsKey("Country") == true)
                    if (miscFilters.ColumnSelector["Country"] == true)
                        dr["Country"] = selectedRecord.Country;
                if (miscFilters.ColumnSelector.ContainsKey("TrustLevel") == true)
                    if (miscFilters.ColumnSelector["TrustLevel"] == true)
                        dr["TrustLevel"] = selectedRecord.TrustLevel;
            }
            else
            {
                dr["Model"] = selectedRecord.Model;
                dr["Brand"] = selectedRecord.Brand;
                dr["Alias Brand"] = selectedRecord.AliasBrand;
                dr["Alias Type"] = selectedRecord.AliasType;
                dr["T/R"] = selectedRecord.IsTarget;
                dr["ID"] = selectedRecord.ID;
                dr["DataSource"] = selectedRecord.DataSource;
                if (miscFilters.SelectTNLink)
                    dr["TN Link"] = selectedRecord.TNLink;

                //dr["Region"] = selectedRecord.Region;
                //dr["Country"] = selectedRecord.Country;
                //dr["DeviceType"] = selectedRecord.DeviceType;

                dr["MainDevice"] = selectedRecord.MainDevice;
                dr["SubDevice"] = selectedRecord.SubDevice;
                dr["Component"] = selectedRecord.Component;
                dr["ComponentStatus"] = selectedRecord.ComponentStatus;
                dr["Region"] = selectedRecord.Region;
                dr["SubRegion"] = selectedRecord.SubRegion;
                dr["Country"] = selectedRecord.Country;
                dr["TrustLevel"] = selectedRecord.TrustLevel;
            }
           
        }
        private DataTable AddDataTableForModelInfoReport(DataSet dataSet, ModelInfoSpecific miscFilters,string tableName)
        {
            if (dataSet != null)
            {
                DataTable dtReport = null;

                dtReport = (String.IsNullOrEmpty(tableName)) ? dataSet.Tables.Add("ModelInfoReport") : dataSet.Tables.Add(tableName);

                switch (miscFilters.ModelInfoOptions)
                {
                    case ModelInfoSpecific.ModelReportOptions.ModelBased:                        
                        dtReport.Columns.Add("Model");
                        dtReport.Columns.Add("Brand");

                        if (miscFilters.ColumnSelector != null)
                        {
                            if (miscFilters.ColumnSelector.ContainsKey("Alias Brand") == true)
                                if (miscFilters.ColumnSelector["Alias Brand"] == true)
                                    dtReport.Columns.Add("Alias Brand");
                            if (miscFilters.ColumnSelector.ContainsKey("Alias Type") == true)
                                if (miscFilters.ColumnSelector["Alias Type"] == true)
                                    dtReport.Columns.Add("Alias Type");
                        }
                        else
                        {
                            dtReport.Columns.Add("Alias Brand");
                            dtReport.Columns.Add("Alias Type");
                        }

                        dtReport.Columns.Add("T/R");
                        dtReport.Columns.Add("ID");
                        //dtReport.Columns.Add("DataSource");
                        if (miscFilters.SelectTNLink)
                            dtReport.Columns.Add("TN Link");
                        break;
                    case ModelInfoSpecific.ModelReportOptions.TNBased:
                        dtReport.Columns.Add("TN");
                        dtReport.Columns.Add("Mode");
                        break;
                    case ModelInfoSpecific.ModelReportOptions.BrandBased:
                        dtReport.Columns.Add("Brand");

                        if (miscFilters.ColumnSelector != null)
                        {
                            if (miscFilters.ColumnSelector.ContainsKey("Alias Brand") == true)
                                if (miscFilters.ColumnSelector["Alias Brand"] == true)
                                    dtReport.Columns.Add("Alias Brand");
                            if (miscFilters.ColumnSelector.ContainsKey("Alias Type") == true)
                                if (miscFilters.ColumnSelector["Alias Type"] == true)
                                    dtReport.Columns.Add("Alias Type");
                        }
                        else
                        {
                            dtReport.Columns.Add("Alias Brand");
                            dtReport.Columns.Add("Alias Type");
                        }

                        dtReport.Columns.Add("Mode");
                        break;
                    case ModelInfoSpecific.ModelReportOptions.IDBased:
                        dtReport.Columns.Add("ID");
                        //dtReport.Columns.Add("Mode");
                        break;

                }
                //default columns
                if (miscFilters.ColumnSelector != null)
                {

                    if (miscFilters.ColumnSelector.ContainsKey("DataSource") == true)                    
                        if (miscFilters.ColumnSelector["DataSource"] == true)
                            dtReport.Columns.Add("DataSource");
                    if (miscFilters.ColumnSelector.ContainsKey("DataSource") == true)
                        if (miscFilters.ColumnSelector.ContainsKey("Region") == true)
                            if (miscFilters.ColumnSelector["Region"] == true)
                                dtReport.Columns.Add("Region");
                    if (miscFilters.ColumnSelector.ContainsKey("SubRegion") == true)
                        if (miscFilters.ColumnSelector["SubRegion"] == true)
                            dtReport.Columns.Add("SubRegion");
                    if (miscFilters.ColumnSelector.ContainsKey("Country") == true)
                        if (miscFilters.ColumnSelector["Country"] == true)
                            dtReport.Columns.Add("Country");
                    //dtReport.Columns.Add("DeviceType");not used after unification implementation
                    if (miscFilters.ColumnSelector.ContainsKey("MainDevice") == true)
                        if (miscFilters.ColumnSelector["MainDevice"] == true)
                            dtReport.Columns.Add("MainDevice");
                    if (miscFilters.ColumnSelector.ContainsKey("SubDevice") == true)
                        if (miscFilters.ColumnSelector["SubDevice"] == true)
                            dtReport.Columns.Add("SubDevice");
                    if (miscFilters.ColumnSelector.ContainsKey("Component") == true)
                        if (miscFilters.ColumnSelector["Component"] == true)
                            dtReport.Columns.Add("Component");
                    if (miscFilters.ColumnSelector.ContainsKey("ComponentStatus") == true)
                        if (miscFilters.ColumnSelector["ComponentStatus"] == true)
                            dtReport.Columns.Add("ComponentStatus");
                    if (miscFilters.ColumnSelector.ContainsKey("TrustLevel") == true)
                        if (miscFilters.ColumnSelector["TrustLevel"] == true)
                            dtReport.Columns.Add("TrustLevel");
                }
                else
                {
                    dtReport.Columns.Add("DataSource");
                    dtReport.Columns.Add("Region");
                    dtReport.Columns.Add("SubRegion");
                    dtReport.Columns.Add("Country");
                    //dtReport.Columns.Add("DeviceType");not used after unification implementation
                    dtReport.Columns.Add("MainDevice");
                    dtReport.Columns.Add("SubDevice");
                    dtReport.Columns.Add("Component");
                    dtReport.Columns.Add("ComponentStatus");
                    dtReport.Columns.Add("TrustLevel");
                }
                return dtReport;
            }
            return null;
        }
        
        #region Exec/ID/TnInfo Report
        public void GetTNAndBrandList(out List<string> tnList, out List<string> brandList)
        {
            tnList = new List<string>();
            brandList = new List<string>();

            foreach (ModelInfoRecord record in this.Items)
            {
                //string formattedTN = "";

                //if (!String.IsNullOrEmpty(record.TNLink))
                //    formattedTN = String.Format("TN{0:D5}", record.TNLink);
                //else
                //    formattedTN = "-";

                if (!tnList.Contains(record.TNLink))
                    tnList.Add(record.TNLink);

                if (!brandList.Contains(record.Brand))
                    brandList.Add(record.Brand);
            }
        }
        public System.Data.DataSet GetModelsByBrand(string brand)
        {
            System.Data.DataSet modelInfoSet = new System.Data.DataSet();
            System.Data.DataTable modelInfoTab = modelInfoSet.Tables.Add();
            modelInfoTab.Columns.Add("Model");
            modelInfoTab.Columns.Add("Branch");//data source
            modelInfoTab.Columns.Add("IsTarget");
            modelInfoTab.Columns.Add("Region");
            modelInfoTab.Columns.Add("Country");
            modelInfoTab.Columns.Add("TN");
            modelInfoTab.Columns.Add("Comment");




            

            foreach (ModelInfoRecord model in this.Items)
            {
                if (model.Brand.Equals(brand, StringComparison.OrdinalIgnoreCase))
                {
                    System.Data.DataRow newRow = modelInfoTab.NewRow();
                    newRow["Model"] = model.Model;
                    newRow["Branch"] = model.DataSource;
                    newRow["IsTarget"] = model.IsTarget;
                    newRow["Region"] = model.Region;
                    newRow["Country"] = model.Country;
                    newRow["TN"] = model.TNLink;
                    newRow["Comment"] = model.Comment;



                    modelInfoTab.Rows.Add(newRow);
                }
            }

            modelInfoSet.AcceptChanges();

            return modelInfoSet;
        }
        public List<string> GetSupportedBrands(string tn)
        {
            List<String> brandList = new List<string>();

            if (!String.IsNullOrEmpty(tn))
            {
                foreach (ModelInfoRecord record in this.Items)
                {
                    if (tn.Equals(record.TNLink, StringComparison.OrdinalIgnoreCase))
                    {
                        if (!brandList.Contains(record.Brand))
                            brandList.Add(record.Brand);
                    }
                }
            }
            else
            {
                foreach (ModelInfoRecord record in this.Items)
                {
                    if(String.IsNullOrEmpty(record.TNLink))
                        if (!brandList.Contains(record.Brand))
                            brandList.Add(record.Brand);
                   
                }
            }

            return brandList;
        }
        #endregion

        #region BrandModelSearch
        /// <summary>
        /// Used by BrandModelSearch.
        /// </summary>
        /// <param name="targetFilter"></param>
        /// <returns></returns>
        public DataSet GetBMSearchResultset(Model.ModelInfoSpecific.IsTargetFilter targetFilter)
        {
            DataSet reportSet = null;

            if (this.Items.Count > 0)
            {
                reportSet = new DataSet("ModelInfo");
                DataTable dtReport = reportSet.Tables.Add("ModelInfoReport");
                dtReport.Columns.Add("Model");
                dtReport.Columns.Add("Brand");
                dtReport.Columns.Add("T/R");
                dtReport.Columns.Add("ID");
                dtReport.Columns.Add("DataSource");
                dtReport.Columns.Add("TN Link");
                dtReport.Columns.Add("Region");
                dtReport.Columns.Add("Country");
                dtReport.Columns.Add("DeviceType");

                string isTargetFilter = String.Empty;
                switch (targetFilter)
                {
                    case Model.ModelInfoSpecific.IsTargetFilter.ShowRemoteModelOnly:
                        isTargetFilter = Model.ModelInfoRecord.REMOTEMODELSTR;
                        break;
                    case Model.ModelInfoSpecific.IsTargetFilter.ShowTargetModelOnly:
                        isTargetFilter = Model.ModelInfoRecord.TARGETMODELSTR;
                        break;
                }

                foreach (Model.ModelInfoRecord record in this.Items)
                {
                    if (!String.IsNullOrEmpty(isTargetFilter))
                    {
                        if (record.IsTarget == isTargetFilter)
                        {
                            DataRow dr = dtReport.NewRow();
                            dr["Brand"] = record.Brand;
                            dr["T/R"] = record.IsTarget;
                            //if (record.IsTarget)
                            //    dr[1] = "(T)";
                            //else
                            //    dr[1] = "(R)";

                            dr["Model"] = record.Model;
                            dr["ID"] = record.ID;
                            dr["DataSource"] = record.DataSource;
                            dr["TN Link"] = record.TNLink;

                            dr["Region"] = record.Region;
                            dr["Country"] = record.Country;
                            dr["DeviceType"] = record.DeviceType;

                            dtReport.Rows.Add(dr);
                        }
                    }
                    else
                    {
                        DataRow dr = dtReport.NewRow();
                        dr["Brand"] = record.Brand;
                        dr["T/R"] = record.IsTarget;
                        //if (record.IsTarget)
                        //    dr[1] = "(T)";
                        //else
                        //    dr[1] = "(R)";

                        dr["Model"] = record.Model;
                        dr["ID"] = record.ID;
                        dr["DataSource"] = record.DataSource;
                        dr["TN Link"] = record.TNLink;
                        dr["Region"] = record.Region;
                        dr["Country"] = record.Country;
                        dr["DeviceType"] = record.DeviceType;


                        dtReport.Rows.Add(dr);
                    }



                }

                reportSet.AcceptChanges();
            }

            return reportSet;
        }

        public DataSet ExportBMSearchResultSet(Model.ModelInfoSpecific.IsTargetFilter targetFilter)
        {
            DataSet reportSet = null;

            if (this.Items.Count > 0)
            {
                Int32 recordCount = 1;
                Int32 worksheetNumber = 1;
                String workSheet = "ModelInfoReport" + "_" + worksheetNumber.ToString();

                reportSet = new DataSet("ModelInfo");
                DataTable dtReport = reportSet.Tables.Add("ModelInfoReport");
                dtReport.Columns.Add("Brand");
                dtReport.Columns.Add("T/R");
                dtReport.Columns.Add("Model");
                dtReport.Columns.Add("ID");
                dtReport.Columns.Add("DataSource");
                dtReport.Columns.Add("TN Link");
                dtReport.Columns.Add("Region");
                dtReport.Columns.Add("Country");
                dtReport.Columns.Add("DeviceType");

                string isTargetFilter = String.Empty;
                switch (targetFilter)
                {
                    case Model.ModelInfoSpecific.IsTargetFilter.ShowRemoteModelOnly:
                        isTargetFilter = Model.ModelInfoRecord.REMOTEMODELSTR;
                        break;
                    case Model.ModelInfoSpecific.IsTargetFilter.ShowTargetModelOnly:
                        isTargetFilter = Model.ModelInfoRecord.TARGETMODELSTR;
                        break;
                }

                foreach (Model.ModelInfoRecord record in this.Items)
                {
                    if (!String.IsNullOrEmpty(isTargetFilter))
                    {
                        if (record.IsTarget == isTargetFilter)
                        {
                            DataRow dr = dtReport.NewRow();
                            dr["Brand"] = record.Brand;
                            dr["T/R"] = record.IsTarget;
                            //if (record.IsTarget)
                            //    dr[1] = "(T)";
                            //else
                            //    dr[1] = "(R)";

                            dr["Model"] = record.Model;
                            dr["ID"] = record.ID;
                            dr["DataSource"] = record.DataSource;
                            dr["TN Link"] = record.TNLink;

                            dr["Region"] = record.Region;
                            dr["Country"] = record.Country;
                            dr["DeviceType"] = record.DeviceType;

                            dtReport.Rows.Add(dr);
                            recordCount++;
                            if (recordCount >= 65536)
                            {
                                reportSet.AcceptChanges();
                                recordCount = 1;
                                worksheetNumber++;
                                workSheet = "ModelInfoReport" + "_" + worksheetNumber.ToString();
                                dtReport = reportSet.Tables.Add(workSheet);
                                dtReport.Columns.Add("Brand");
                                dtReport.Columns.Add("T/R");
                                dtReport.Columns.Add("Model");
                                dtReport.Columns.Add("ID");
                                dtReport.Columns.Add("DataSource");
                                dtReport.Columns.Add("TN Link");
                                dtReport.Columns.Add("Region");
                                dtReport.Columns.Add("Country");
                                dtReport.Columns.Add("DeviceType");
                            }
                        }
                    }
                    else
                    {
                        DataRow dr = dtReport.NewRow();
                        dr["Brand"] = record.Brand;
                        dr["T/R"] = record.IsTarget;
                        //if (record.IsTarget)
                        //    dr[1] = "(T)";
                        //else
                        //    dr[1] = "(R)";

                        dr["Model"] = record.Model;
                        dr["ID"] = record.ID;
                        dr["DataSource"] = record.DataSource;
                        dr["TN Link"] = record.TNLink;
                        dr["Region"] = record.Region;
                        dr["Country"] = record.Country;
                        dr["DeviceType"] = record.DeviceType;


                        dtReport.Rows.Add(dr);
                        recordCount++;
                        if (recordCount >= 65536)
                        {
                            reportSet.AcceptChanges();
                            recordCount = 1;
                            worksheetNumber++;
                            workSheet = "ModelInfoReport" + "_" + worksheetNumber.ToString();
                            dtReport = reportSet.Tables.Add(workSheet);
                            dtReport.Columns.Add("Brand");
                            dtReport.Columns.Add("T/R");
                            dtReport.Columns.Add("Model");
                            dtReport.Columns.Add("ID");
                            dtReport.Columns.Add("DataSource");
                            dtReport.Columns.Add("TN Link");
                            dtReport.Columns.Add("Region");
                            dtReport.Columns.Add("Country");
                            dtReport.Columns.Add("DeviceType");
                        }
                    }



                }

                reportSet.AcceptChanges();
            }

            return reportSet;
        }
        #endregion
    }
    public class QuickSetReportModel : Collection<QuickSetReportRecord>
    {

        internal BrandInfo GetBrandInfo(String modeList, String deviceName, String brand)
        {
            foreach (QuickSetReportRecord item in this.Items)
            {
                BrandInfo brandSelected = GetBrands(item.BrandList,brand);
                if (brandSelected != null && item.DeviceCode == modeList && item.DeviceName == deviceName)
                {
                    return brandSelected;
                }
            }
            return null;
        }
        private BrandInfo GetBrands(List<BrandInfo> brandList, String brand)
        {
            if(brandList != null)
                foreach (BrandInfo item in brandList)
                {
                    if (item.Name == brand)
                        return item;
                }
            return null;
        }
    }   
    public class QuickSetReportRecord
    {
        private String m_DeviceNo = "0";
        public String DeviceNo
        {
            get { return m_DeviceNo; }
            set { m_DeviceNo = value; }
        }
        private String m_DeviceCode = String.Empty;
        public String DeviceCode
        {
            get { return m_DeviceCode; }
            set { m_DeviceCode = value; }
        }
        private String m_DeviceName = String.Empty;
        public String DeviceName
        {
            get { return m_DeviceName; }
            set { m_DeviceName = value; }
        }
        private List<BrandInfo> m_BrandList = new List<BrandInfo>();
        public List<BrandInfo> BrandList
        {
            get { return m_BrandList; }
            set { m_BrandList = value; }
        }
    }
    public class DataSourceInfo
    {
        private String m_Name = String.Empty;
        public String Name
        {
            get { return m_Name; }
            set { m_Name = value; }
        }
    }
    public class ModelInfo
    {
        private String _name = String.Empty;
        public String Name
        {
            get { return _name; }
            set { _name = value; }
        }
        private Int32 _modelCount = 0;
        public Int32 ModelCount
        {
            get { return _modelCount; }
            set { _modelCount = value; }
        }
        private List<DataSourceInfo> m_DataSources = new List<DataSourceInfo>();
        public List<DataSourceInfo> DataSources
        {
            get { return m_DataSources; }
            set { m_DataSources = value; }
        }
    }
    public class BrandInfo
    {
        private String m_DeviceTypeFlag = String.Empty;
        public String DeviceTypeFlag
        {
            get { return m_DeviceTypeFlag; }
            set { m_DeviceTypeFlag = value; }
        }
        private String _Name = String.Empty;
        public String Name
        {
            get { return _Name; }
            set { _Name = value; }
        }
        private int _ModelCount;
        public int ModelCount
        {
            get { return _ModelCount; }
            set { _ModelCount = value; }
        }
        private Int32 _BrandNumber;
        public Int32 BrandNumber
        {
            get { return _BrandNumber; }
            set { _BrandNumber = value; ;}
        }
        //added for quickset report
        private List<IDInfo> m_IDList = new List<IDInfo>();
        public List<IDInfo> IDList
        {
            get { return m_IDList; }
            set { m_IDList = value; }
        }
    }
    public class IDInfo
    {
        private String id = String.Empty;
        public String ID
        {
            get { return id; }
            set { id = value; }
        }
        private String lowPriorityID = String.Empty;
        public String LowPriorityID
        {
            get { return lowPriorityID; }
            set { lowPriorityID = value; }
        }
        private IList<ModelInfo> _modelList = new List<ModelInfo>();
        public IList<ModelInfo> ModelList
        {
            get { return _modelList; }
            set { _modelList = value; }
        }
        private Int32 modelCount = 0;
        public Int32 ModelCount
        {
            get { return modelCount; }
            set { modelCount = value; }
        }
        private double totalUnitssold = 0;
        public double TotalUnitsSold
        {
            get { return totalUnitssold; }
            set { totalUnitssold = value; }
        }
        private int searchstatisticsCount;
        public int SearchStatisticsCount
        {
            get { return searchstatisticsCount; }
            set { searchstatisticsCount = value; }
        }
        private IDPrefixInfoCollection prefixCollection;
        public IDPrefixInfoCollection Prefixes
        {
            get { return prefixCollection; }
            set { prefixCollection = value; }
        }
        private ModelInfoRecordCollection modelInfoRecordCollection;
        public ModelInfoRecordCollection ModelRecords
        {
            get { return modelInfoRecordCollection; }
            set { modelInfoRecordCollection = value; }
        }
        private int executorCode;
        public int Executor
        {
            get { return executorCode; }
            set { executorCode = value; }
        }
        //Brand Availability againest ID
        private String m_UEI2Avail = String.Empty;
        public String UEI2Avail
        {
            get { return m_UEI2Avail; }
            set { m_UEI2Avail = value; }
        }
        private String m_POSAvail = String.Empty;
        public String POSAvail
        {
            get { return m_POSAvail; }
            set { m_POSAvail = value; }
        }
        private String m_OSSAvail = String.Empty;
        public String OSSAvail
        {
            get { return m_OSSAvail; }
            set { m_OSSAvail = value; }
        }
        
    }
    public class DeviceInfo
    {
        /// <summary>
        /// no more used after unification
        /// </summary>
        private String deviceID = String.Empty;
        public String DeviceID
        {
            get { return deviceID; }
            set { deviceID = value; }
        }
        /// <summary>
        /// no more used after unification
        /// </summary>
        private String name = String.Empty;
        public String Name
        {
            get { return name; }
            set { name = value; }
        }
        private String _MainDevice = String.Empty;
        public String MainDevice
        {
            get { return _MainDevice; }
            set { _MainDevice = value; }
        }
        private String _SubDevice = String.Empty;
        public String SubDevice
        {
            get { return _SubDevice; }
            set { _SubDevice = value; }
        }
        private String _Component = String.Empty;
        public String Component
        {
            get { return _Component; }
            set { _Component = value; }
        }
        private String _RegionalName = String.Empty;
        public String RegionalName
        {
            get { return _RegionalName; }
            set { _RegionalName = value; }
        }
        private List<BrandInfo> _BrandList = new List<BrandInfo>();
        public List<BrandInfo> BrandList
        {
            get { return _BrandList; }
            set { _BrandList = value; }
        }
        public BrandInfo GetBrandInfo(string brandName)
        {
            if (!String.IsNullOrEmpty(brandName))
            {
                foreach (BrandInfo brand in this._BrandList)
                {
                    if (brand.Name == brandName)
                        return brand;
                }
            }

            return null;
        }
        private IList<IDInfo> _idList = new List<IDInfo>();
        public IList<IDInfo> IdList
        {
            get { return _idList; }
            set { _idList = value; }
        }
    }
    public class UnsupportedIdModel
    {
        private Hashtable items = new Hashtable();
        public void AddId(String id)
        {
            if (!String.IsNullOrEmpty(id))
            {
                String mode = id.Substring(0, 1);

                if (!items.ContainsKey(mode))
                {
                    List<String> idList = new List<String>();
                    idList.Add(id);

                    items.Add(mode, idList);
                }
                else
                {
                    List<String> idList = items[mode] as List<String>;
                    if (!idList.Contains(id))
                        idList.Add(id);
                }
            }
        }
        public List<String> GetDistinctModes()
        {
            List<String> modeList = new List<String>();
            foreach (object key in this.items.Keys)
            {
                modeList.Add(key.ToString());
            }

            modeList.Sort();

            return modeList;
        }
        public List<String> GetIdsByMode(String mode)
        {
            if (!String.IsNullOrEmpty(mode))
            {
                return this.items[mode] as List<String>;
            }

            return null;
        }
        public List<String> GetAllIds()
        {
            List<String> allIdList = new List<String>();

            foreach (Object item in this.items.Values)
            {
                List<String> idList = item as List<String>;
                allIdList.AddRange(idList);
            }

            return allIdList;
        }
    }
    public class RestrictedIdModel
    {
        private System.Collections.Hashtable items = new System.Collections.Hashtable();
        public void AddId(string id)
        {
            if (!String.IsNullOrEmpty(id))
            {
                string mode = id.Substring(0, 1);

                if (!items.ContainsKey(mode))
                {
                    List<string> idList = new List<string>();
                    idList.Add(id);

                    items.Add(mode, idList);
                }
                else
                {
                    List<string> idList = items[mode] as List<string>;
                    if(!idList.Contains(id))
                        idList.Add(id);
                }
            }
        }
        public List<string> GetDistinctModes()
        {
            List<string> modeList = new List<string>();
            foreach (object key in this.items.Keys)
            {
                modeList.Add(key.ToString());
            }
            
            modeList.Sort();
            
            return modeList;
        }
        public List<string> GetIdsByMode(string mode)
        {
            if (!String.IsNullOrEmpty(mode))
            {
               return this.items[mode] as List<string>;
            }

            return null;
        }
        public List<string> GetAllIds()
        {
            List<string> allIdList = new List<string>();
            
            foreach (object item in this.items.Values)
            {
                List<string> idList = item as List<string>;
                allIdList.AddRange(idList);
            }

            return allIdList; 
        }
    }   
    public class Empty_NonEmptyIdModel
    {
        private Hashtable items = new Hashtable();

        public Empty_NonEmptyIdModel(bool isForEmptyIdReport)
        {
            this.isEmptyIdModel = isForEmptyIdReport;
        }

        private bool isEmptyIdModel = false;
        public Boolean IsEmptyIdModel
        {
           get { return this.isEmptyIdModel; }
        }

        public void AddId(string id)
        {
            if (!String.IsNullOrEmpty(id))
            {
                string mode = id.Substring(0, 1);

                if (!items.ContainsKey(mode))
                {
                    List<string> idList = new List<string>();
                    idList.Add(id);

                    items.Add(mode, idList);
                }
                else
                {
                    List<string> idList = items[mode] as List<string>;
                    if (!idList.Contains(id))
                        idList.Add(id);
                }
            }
        }
        public List<string> GetDistinctModes()
        {
            List<string> modeList = new List<string>();
            foreach (object key in this.items.Keys)
            {
                modeList.Add(key.ToString());
            }

            modeList.Sort();

            return modeList;
        }
        public List<string> GetIdsByMode(string mode)
        {
            if (!String.IsNullOrEmpty(mode))
            {
                return this.items[mode] as List<string>;
            }

            return null;
        }
        public List<string> GetAllIds()
        {
            List<string> allIdList = new List<string>();

            foreach (object item in this.items.Values)
            {
                List<string> idList = item as List<string>;
                allIdList.AddRange(idList);
            }

            return allIdList;
        }
    }
    public class SynthesizerTableModel
    {
        private Dictionary<String, List<SynthesizerTable>> m_Items = new Dictionary<String, List<SynthesizerTable>>();
        public Dictionary<String, List<SynthesizerTable>> Items
        {
            get { return m_Items; }
            set { m_Items = value; }
        }
        public void AddSynthesizer(SynthesizerTable m_Synthesizer)
        {
            if(!String.IsNullOrEmpty(m_Synthesizer.ID))
            {
                String m_Mode = m_Synthesizer.ID.Substring(0, 1);
                if(!Items.ContainsKey(m_Mode))
                {
                    List<SynthesizerTable> m_SynthesizerList = new List<SynthesizerTable>();
                    m_SynthesizerList.Add(m_Synthesizer);
                    Items.Add(m_Mode, m_SynthesizerList);                    
                }
                else
                {
                    List<SynthesizerTable> m_SynthesizerList = Items[m_Mode] as List<SynthesizerTable>;
                    if(!m_SynthesizerList.Contains(m_Synthesizer))
                        m_SynthesizerList.Add(m_Synthesizer);
                }
            }
        }
        public List<String> GetDistinctModes()
        {
            List<String> m_ModeList = new List<String>();
            foreach(String m_Key in this.Items.Keys)
            {
                m_ModeList.Add(m_Key.ToString());
            }
            m_ModeList.Sort();
            return m_ModeList;
        }
        public List<SynthesizerTable> GetSynthesizersByMode(String m_Mode)
        {
            if(!String.IsNullOrEmpty(m_Mode))
            {
                return this.Items[m_Mode] as List<SynthesizerTable>;
            }
            return null;
        }
        public List<SynthesizerTable> GetAllSynthesizers()
        {
            List<SynthesizerTable> m_AllSynthesizerList = new List<SynthesizerTable>();
            foreach(Object item in this.Items.Values)
            {
                List<SynthesizerTable> m_SynthesizerList = item as List<SynthesizerTable>;
                m_AllSynthesizerList.AddRange(m_SynthesizerList);
            }
            return m_AllSynthesizerList;
        }
    }
    public class IDPrefixInfo
    {
        private string data;
        public string Data
        {
            get { return data; }
            set { data = value; }
        }
        private string description;
        public string Description
        {
            get { return description; }
            set { description = value; }
        }
    }
    public class IDPrefixInfoCollection : Collection<IDPrefixInfo>
    {
        public System.Data.DataSet GetDataSet()
        {
           System.Data.DataSet dataSet = null;

           if (this.Items.Count > 0)
           {
               dataSet = new System.Data.DataSet("IDPrefixSet");
               System.Data.DataTable table = dataSet.Tables.Add();
               table.Columns.Add("Data");
               table.Columns.Add("Prefix");

               foreach (IDPrefixInfo prefix in this.Items)
               {
                   System.Data.DataRow row = table.NewRow();
                   row["Data"] = prefix.Data;
                   row["Prefix"] = prefix.Description;

                   table.Rows.Add(row);
               }

               dataSet.AcceptChanges();
           }
            return dataSet;
            
            
        }
    }
    public class LabelIntronSearchRecord
    {
        private string _ID;
        public string ID
        {
            get { return _ID; }
            set { _ID = value; }
        }
        private string _Data;
        public string Data
        {
            get { return _Data; }
            set { _Data = value; }
        }
        private string _Synth;
        public string Synth
        {
            get { return _Synth; }
            set { _Synth = value; }
        }
        private string _Label;
        public string Label
        {
            get { return _Label; }
            set { _Label = value; }
        }
        private string _Intron;
        public string Intron
        {
            get { return _Intron; }
            set { _Intron = value; }
        }
        private string _Priority;
        public string Priority
        {
            get { return _Priority; }
            set { _Priority = value; }
        }
    }
    public class LabelIntronSearchReport : IDistinctView
    {
        //private System.Collections.Hashtable items = new System.Collections.Hashtable();
        private SortedDictionary<string, List<LabelIntronSearchRecord>> items = new SortedDictionary<string, List<LabelIntronSearchRecord>>();
        public LabelIntronSearchReport(bool isIntronReport)
        {
            this._IsIntronReport = isIntronReport;
        }
        private bool _IsIntronReport;
        public bool IsIntronReport
        {
            get { return _IsIntronReport; }
            set { _IsIntronReport = value; }
        }
        public void AddRecord(LabelIntronSearchRecord record)
        {
            if (record != null && !String.IsNullOrEmpty(record.ID))
            {
                string mode = record.ID.Substring(0, 1);

                if (!items.ContainsKey(mode))
                {
                    //add the mode and record associated
                    List<LabelIntronSearchRecord> dataList = new List<LabelIntronSearchRecord>();
                    dataList.Add(record);

                    //add to underlying hashtable
                    items.Add(mode,dataList);
                }
                else
                {
                    List<LabelIntronSearchRecord> dataList = items[mode];
                    dataList.Add(record);
                }
            }
        }
        public System.Data.DataSet GetReportForDisplay(IDModeRecordModel allModeNames,string tableName)
        {
            System.Data.DataSet dataSet = new System.Data.DataSet();

            if (this.items.Count > 0)
            {
                //string reportName = "LabelSearchReport";
                //if (this._IsIntronReport)
                //    reportName = "IntronSearchReport";

                System.Data.DataTable dataTable = AddTable(dataSet, tableName);
                
                //get all the keys(modes)
                foreach (string mode in this.items.Keys)
                {
                    //give header
                    string modeName = allModeNames.getModeName(mode);

                    System.Data.DataRow row = dataTable.NewRow();
                    row[0] = modeName;
                    dataTable.Rows.Add(row);

                    //add the data
                    List<LabelIntronSearchRecord> dataList = this.items[mode];

                    foreach (LabelIntronSearchRecord record in dataList)
                    {
                        row = dataTable.NewRow();
                        row["ID"] = record.ID;
                        row["Data"] = record.Data;
                        row["Synth"] = record.Synth;
                        row["Label"] = record.Label;
                        row["Intron"] = record.Intron;
                        row["Priority"] = record.Priority;

                        dataTable.Rows.Add(row);
                    }                                                           
                }
                dataSet.AcceptChanges();                
            }

            return dataSet;
        }
        public System.Data.DataSet GetReportForDisplay(IDModeRecordModel allModeNames, string tableName,DisplayFilter displayMode)
        {
            if (this.items.Count > 0)
            {
                if (displayMode == DisplayFilter.Distinct)
                    return GetDistinctReportForDisplay(allModeNames, tableName);

                return GetReportForDisplay(allModeNames, tableName);
            }

            return null;
        }
        public System.Data.DataSet GetDistinctReportForDisplay(IDModeRecordModel allModeNames, string tableName)
        {
            System.Data.DataSet dataSet = new System.Data.DataSet();
            
            if (this.items.Count > 0)
            {
                System.Data.DataTable dataTable = dataSet.Tables.Add(tableName);
                dataTable.Columns.Add("ID");

                foreach (string mode in this.items.Keys)
                {
                    //give header
                    string modeName = allModeNames.getModeName(mode);

                    System.Data.DataRow row = dataTable.NewRow();
                    row[0] = modeName;
                    dataTable.Rows.Add(row);

                    //get the data under the mode(device)
                    List<LabelIntronSearchRecord> dataList = this.items[mode];
                    //get the distinct ids only from the data list.Use a list object for removing duplicates
                    List<string> avoidDuplicates = new List<string>();
                    foreach (LabelIntronSearchRecord record in dataList)
                        if (!avoidDuplicates.Contains(record.ID))
                            avoidDuplicates.Add(record.ID);

                    //add all the distinct ids to datatable.
                    foreach (string id in avoidDuplicates)
                    {
                        row = dataTable.NewRow();
                        row[0] = id;
                        dataTable.Rows.Add(row);
                    }

               }

               dataTable.AcceptChanges();
            }

            return dataSet;
        }
        public System.Data.DataSet GetReportForExcel(DisplayFilter displayMode)
        {
            if (displayMode == DisplayFilter.Distinct)
                return GetDistinctIDReportForExcel();

            return GetReportForExcel();
        }
        public System.Data.DataSet GetReportForExcel()
        {
            System.Data.DataSet dataSet = new System.Data.DataSet("LabelIntron");
            
            if (this.items.Count > 0)
            {
                foreach (string mode in this.items.Keys)
                {
                    //create data table
                    System.Data.DataTable dataTable = AddTable(dataSet, mode);

                    //add the data
                    List<LabelIntronSearchRecord> dataList = this.items[mode];
                    //for tracking the number of records.One worksheet do allow more than 65536 records.
                    Int32 recordCount = 1;
                    Int32 worksheetNumber = 1;
                    
                    foreach (LabelIntronSearchRecord record in dataList)
                    {
                        System.Data.DataRow row = dataTable.NewRow();
                        row["ID"] = record.ID;
                        row["Data"] = record.Data;
                        row["Synth"] = record.Synth;
                        row["Label"] = record.Label;
                        row["Intron"] = record.Intron;
                        row["Priority"] = record.Priority;

                        dataTable.Rows.Add(row);

                        recordCount++;

                        if (recordCount >= 65536)
                        {
                            //add new table
                            dataSet.AcceptChanges();
                            recordCount = 1;
                            worksheetNumber++;

                            dataTable = AddTable(dataSet, mode + worksheetNumber.ToString());

                        }
                    }
                }

                dataSet.AcceptChanges();
            }

            return dataSet;
        }
        private System.Data.DataSet GetDistinctIDReportForExcel()
        {
            System.Data.DataSet dataSet = new System.Data.DataSet("LabelIntron");
            SortedList<string, List<string>> distinctIDList = GetDistinctIDList();

            foreach (string mode in distinctIDList.Keys)
            {
                //create data table
                System.Data.DataTable dataTable = dataSet.Tables.Add(mode);
                dataTable.Columns.Add("ID");

                //add the data
                List<string> dataList = distinctIDList[mode];
                //for tracking the number of records.One worksheet do allow more than 65536 records.
                Int32 recordCount = 1;
                Int32 worksheetNumber = 1;

                foreach (string id in dataList)
                {
                    System.Data.DataRow row = dataTable.NewRow();
                    row["ID"] = id;

                    dataTable.Rows.Add(row);

                    recordCount++;

                    if (recordCount >= 65536)
                    {
                        //add new table
                        dataSet.AcceptChanges();
                        recordCount = 1;
                        worksheetNumber++;

                        dataTable = dataSet.Tables.Add(mode + worksheetNumber.ToString());

                    }
                }

            }

            dataSet.AcceptChanges();
            return dataSet;
        }                
        public System.Data.DataSet GetReportForTextFile(IDModeRecordModel allModeNames, DisplayFilter displayMode)
        {
            if (displayMode == DisplayFilter.Distinct)
                return GetDistinctReportForTextFile(allModeNames);

            return GetReportForTextFile(allModeNames);

        }        
        public System.Data.DataSet GetReportForTextFile(IDModeRecordModel allModeNames)
        {
            System.Data.DataSet dataSet = new System.Data.DataSet("LabelIntron");

            if (this.items.Count > 0)
            {
                foreach (string mode in this.items.Keys)
                {
                    //create data table
                    System.Data.DataTable dataTable = AddTable(dataSet, allModeNames.getModeName(mode));

                    //add the data
                    List<LabelIntronSearchRecord> dataList = this.items[mode];
                    //for tracking the number of records.One worksheet do allow more than 65536 records.
                    

                    foreach (LabelIntronSearchRecord record in dataList)
                    {
                        System.Data.DataRow row = dataTable.NewRow();
                        row["ID"] = record.ID;
                        row["Data"] = record.Data;
                        row["Synth"] = record.Synth;
                        row["Label"] = record.Label;
                        row["Intron"] = record.Intron;
                        row["Priority"] = record.Priority;

                        dataTable.Rows.Add(row);



                    }
                }

                dataSet.AcceptChanges();
            }

            return dataSet;
        }
        private System.Data.DataSet GetDistinctReportForTextFile(IDModeRecordModel allModeNames)
        {
            System.Data.DataSet dataSet = new System.Data.DataSet("LabelIntron");
            SortedList<string, List<string>> distinctIDList = GetDistinctIDList();

            foreach (string mode in distinctIDList.Keys)
            {
                System.Data.DataTable dataTable = dataSet.Tables.Add(mode);
                dataTable.Columns.Add("ID");

                //add the data
                List<string> dataList = distinctIDList[mode];
                //for tracking the number of records.One worksheet do allow more than 65536 records.


                foreach (string id in dataList)
                {
                    System.Data.DataRow row = dataTable.NewRow();
                    row["ID"] = id;
                    
                    dataTable.Rows.Add(row);

                }

            }

            dataSet.AcceptChanges();

            return dataSet;
        }        
        private static System.Data.DataTable AddTable(System.Data.DataSet dataSet,string tableName)
        {
            System.Data.DataTable dataTable = null;
            //Allows null dataset.
            if (dataSet != null)
                dataTable = dataSet.Tables.Add(tableName);
            else
                dataTable = new DataTable(tableName);

            dataTable.Columns.Add("ID");
            dataTable.Columns.Add("Data");
            dataTable.Columns.Add("Synth");
            dataTable.Columns.Add("Label");
            dataTable.Columns.Add("Intron");
            dataTable.Columns.Add("Priority");

            return dataTable;
        }
        private SortedList<string, List<string>> GetDistinctIDList()
        {
            SortedList<string, List<string>> distinctData = new SortedList<string, List<string>>();

            if (this.items.Count > 0)
            {
                foreach (string mode in this.items.Keys)
                {
                    List<string> idList = new List<string>();
                    List<LabelIntronSearchRecord> dataList = this.items[mode];
                    foreach (LabelIntronSearchRecord record in dataList)
                        if (!idList.Contains(record.ID))
                            idList.Add(record.ID);

                    distinctData.Add(mode, idList);
                }
            }

            return distinctData;
        }
        private SortedList<string, DataTable> GetDataGroupByID()
        {
            SortedList<string, DataTable> dataGroupedById = new SortedList<string, DataTable>();

            if (this.items.Count > 0)
            {
                foreach (string mode in this.items.Keys)
                {
                    //get the data under the mode
                    List<LabelIntronSearchRecord> data = this.items[mode];

                    foreach (LabelIntronSearchRecord record in data)
                    {
                        DataTable dtLabelIntronForId = null;
                        if (dataGroupedById.ContainsKey(record.ID))
                            dtLabelIntronForId = dataGroupedById[record.ID];
                        else
                        {
                            dtLabelIntronForId = new DataTable();
                            dtLabelIntronForId.Columns.Add("Data");
                            dtLabelIntronForId.Columns.Add("Synth");
                            dtLabelIntronForId.Columns.Add("Label");
                            dtLabelIntronForId.Columns.Add("Intron");
                            dtLabelIntronForId.Columns.Add("Priority");
                            dataGroupedById.Add(record.ID, dtLabelIntronForId);
                        }

                        DataRow row = dtLabelIntronForId.NewRow();
                        row["Data"] = record.Data;
                        row["Synth"] = record.Synth;
                        row["Label"] = record.Label;
                        row["Intron"] = record.Intron;
                        row["Priority"] = record.Priority;

                        dtLabelIntronForId.Rows.Add(row);

                    }
                }
            }

            return dataGroupedById;
        }
        #region IDistinctView Members

        SortedList<string, List<string>> IDistinctView.DistinctViewData
        {
            get
            {
                return GetDistinctIDList();
            }
        }

        SortedList<string, DataTable> IDistinctView.DetailsViewData
        {
            get { return GetDataGroupByID(); }
        }

        #endregion       
    
        public LabelIntronSearchReport SearchSecondryKeyLabels(LabelIntronSearchReport secondrySearch, String searchMode)
        {            
            Dictionary<String, String> strlblData = new Dictionary<String, String>();
            LabelIntronSearchReport result = null;
            switch (searchMode)
            {
                case "LabelSearch":   
                    if (secondrySearch != null)
                    {
                        foreach (String item in this.items.Keys)
                        {
                            if (this.items[item] != null)
                            {
                                foreach (LabelIntronSearchRecord lblint in this.items[item])
                                {
                                    if (secondrySearch.items.ContainsKey(item))
                                    {
                                        foreach (LabelIntronSearchRecord templblInt in secondrySearch.items[item])
                                        {
                                            if (templblInt.Label == lblint.Label)
                                                if (!strlblData.ContainsKey(lblint.Label))
                                                {
                                                    strlblData.Add(lblint.Label, lblint.Data);
                                                }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    if (this.items != null && strlblData.Count > 0)
                    {

                        foreach (String item in this.items.Keys)
                        {
                            if (result == null)
                                result = new LabelIntronSearchReport(false);
                            if (this.items[item] != null)
                            {
                                foreach (LabelIntronSearchRecord lblint in this.items[item])
                                {
                                    foreach (String templblInt in strlblData.Keys)
                                    {
                                        if ((strlblData[templblInt] == lblint.Data) && (templblInt != lblint.Label))
                                            result.AddRecord(lblint);
                                    }
                                }
                            }
                        }
                    }
                    break;
                case "IntronSearch":
                       if (secondrySearch != null)
                    {
                        foreach (String item in this.items.Keys)
                        {
                            if (this.items[item] != null)
                            {
                                foreach (LabelIntronSearchRecord lblint in this.items[item])
                                {
                                    if (secondrySearch.items.ContainsKey(item))
                                    {
                                        foreach (LabelIntronSearchRecord templblInt in secondrySearch.items[item])
                                        {
                                            if (templblInt.Intron == lblint.Intron)
                                                if (!strlblData.ContainsKey(lblint.Intron))
                                                {
                                                    strlblData.Add(lblint.Intron, lblint.Data);
                                                }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    if (this.items != null && strlblData.Count > 0)
                    {

                        foreach (String item in this.items.Keys)
                        {
                            if (result == null)
                                result = new LabelIntronSearchReport(false);
                            if (this.items[item] != null)
                            {
                                foreach (LabelIntronSearchRecord lblint in this.items[item])
                                {
                                    foreach (String templblInt in strlblData.Keys)
                                    {
                                        if ((strlblData[templblInt] == lblint.Data) && (templblInt != lblint.Intron))
                                            result.AddRecord(lblint);
                                    }
                                }
                            }
                        }
                    }
                    break;
            }
            return result;
        }
    }

    public class IntronLabelDictionary
    {
        private Dictionary<string, List<string[]>> labelIntronDict = new Dictionary<string, List<string[]>>();
        private const string emptyCategory = "NOCATEGORY";
        private readonly string[] iLDictKeys = new string[] { "Label", "Intron", "Category" };
       
        public IntronLabelDictionary()
        {
            
        }


        public static string UnassignedCategory
        {
            get { return emptyCategory; }
        }
       

        public void AddItem(System.Collections.Hashtable item)
        {
            if (item != null)
            {
                string category = Convert.ToString(item[iLDictKeys[2]]);
                if (String.IsNullOrEmpty(category))
                    category = emptyCategory;

                string[] labelWithIntron = null;
                if (labelIntronDict.ContainsKey(category))//if category exists
                {
                    labelWithIntron = new string[2];
                    labelWithIntron[0] = Convert.ToString(item[iLDictKeys[0]]);
                    labelWithIntron[1] = Convert.ToString(item[iLDictKeys[1]]);

                    this.labelIntronDict[category].Add(labelWithIntron);

                }
                else//if category has not been added
                {
                    labelWithIntron = new string[2];
                    labelWithIntron[0] = Convert.ToString(item[iLDictKeys[0]]);
                    labelWithIntron[1] = Convert.ToString(item[iLDictKeys[1]]);

                    List<string[]> list = new List<string[]>();
                    list.Add(labelWithIntron);

                    this.labelIntronDict.Add(category, list);
                }
            }
        }

        public List<string> GetCategories()
        {
            if (this.labelIntronDict.Count > 0)
            {
                List<string> categoryList = new List<string>();
                foreach (string category in this.labelIntronDict.Keys)
                    categoryList.Add(category);

                categoryList.Sort();
                return categoryList;
            }

            return new List<string>();
        }


        public DataTable GetLabelIntronSet(string category,string searchPattern)
        {
            if (!String.IsNullOrEmpty(searchPattern))
            {
                DataTable intronLabelTab = new DataTable();
                intronLabelTab.Columns.Add(iLDictKeys[0]);
                intronLabelTab.Columns.Add(iLDictKeys[1]);

                DataTable intronLabelData = GetLabelIntronSet(category);

                foreach (DataRow row in intronLabelData.Rows)
                {
                    //compare the strings here.compare label first.
                    string label = row[iLDictKeys[0]].ToString();
                    string intron = row[iLDictKeys[1]].ToString();

                    if (label.StartsWith(searchPattern, StringComparison.OrdinalIgnoreCase) || intron.StartsWith(searchPattern, StringComparison.OrdinalIgnoreCase))
                    {
                        DataRow newRow = intronLabelTab.NewRow();
                        newRow.ItemArray = row.ItemArray;
                        intronLabelTab.Rows.Add(newRow);
                    }
                  
                    
                }

                return intronLabelTab;
            }
            else
            {
                return GetLabelIntronSet(category);
            }

        }

        public DataTable GetLabelIntronSet(string category)
        {
            DataTable intronLabelTab = new DataTable();
            intronLabelTab.Columns.Add(iLDictKeys[0]);
            intronLabelTab.Columns.Add(iLDictKeys[1]);

            if (String.IsNullOrEmpty(category))
            {
                foreach (string key in this.labelIntronDict.Keys)
                {
                   List<string[]> intronLabelList = this.labelIntronDict[key];
                   foreach (string[] labelIntron in intronLabelList)
                   {
                       DataRow row = intronLabelTab.NewRow();
                       row.ItemArray = labelIntron;
                       intronLabelTab.Rows.Add(row);
                   }
                }
            }
            else
            {
                List<string[]> intronLabelList = this.labelIntronDict[category];
                foreach (string[] labelIntron in intronLabelList)
                {
                    DataRow row = intronLabelTab.NewRow();
                    row.ItemArray = labelIntron;
                    intronLabelTab.Rows.Add(row);
                }
            }

            return intronLabelTab;
        }
    }
    public interface IDistinctView
    {
        SortedList<string, List<string>> DistinctViewData
        {
            get;
        }
        SortedList<string, DataTable> DetailsViewData
        {
            get;
        }
    }
    public class IDComparer : IComparer<IDInfo>
    {
        private ReportSortBy sortBy = ReportSortBy.ModelCount;
        public IDComparer(ReportSortBy sortBy)
        {
            this.sortBy = sortBy;
        }
        public Int32 Compare(IDInfo x, IDInfo y)
        {
            if (this.sortBy == ReportSortBy.ModelCount)
            {
                if (y.ModelCount == x.ModelCount)
                {
                    return x.ID.CompareTo(y.ID);
                }                
                return y.ModelCount.CompareTo(x.ModelCount);
            }
            if (this.sortBy == ReportSortBy.POS)
            {
                if (y.TotalUnitsSold == x.TotalUnitsSold)
                {
                    return x.ID.CompareTo(y.ID);
                }
                return y.TotalUnitsSold.CompareTo(x.TotalUnitsSold);
            }
            if (this.sortBy == ReportSortBy.SearchStatistics)
            {
                if (y.SearchStatisticsCount == x.SearchStatisticsCount)
                {
                    return x.ID.CompareTo(y.ID);
                }
                return y.SearchStatisticsCount.CompareTo(x.SearchStatisticsCount);
            }
            return x.ID.CompareTo(y.ID);
        }
    }
    public class BrandComparer : IComparer<BrandInfo>
    {
        private ReportSortBy _SortBy;
        private string _propertyName = String.Empty;

        public BrandComparer(ReportSortBy sortBy)
        {
            this._SortBy = sortBy;
        }
        #region IComparer<Brand> Members
        public int Compare(BrandInfo x, BrandInfo y)
        {
            if (this._SortBy == ReportSortBy.ModelCount)
            {
                if (y.ModelCount == x.ModelCount)
                {
                    return x.Name.CompareTo(y.Name);
                }
                return y.ModelCount.CompareTo(x.ModelCount);
            }
            return x.Name.CompareTo(y.Name);
        }
        #endregion
    }
    public class TNComparer : IComparer<TNInfo>
    {
        #region IComparer<TNInfo> Members
        public int Compare(TNInfo x, TNInfo y)
        {
            return x.TN.CompareTo(y.TN);
        }
        #endregion
    }
    public class ModelComparer : IComparer<ModelInfo>
    {
        private ReportSortBy _SortBy;
        public ModelComparer(ReportSortBy sortBy)
        {
            this._SortBy = sortBy;
        }
        #region IComparer<ModelInfo> Members
        public int Compare(ModelInfo x, ModelInfo y)
        {
            if (this._SortBy == ReportSortBy.ModelCount)
            {
                if (y.ModelCount == x.ModelCount)
                    return x.Name.CompareTo(y.Name);
                return y.ModelCount.CompareTo(x.ModelCount);
            }
            return x.Name.CompareTo(y.Name);
        }
        #endregion
    }
}